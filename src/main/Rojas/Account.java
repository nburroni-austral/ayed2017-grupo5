package main.Rojas;

/**
 * Created by JoseRojas on 25/4/17.
 */
public class Account implements Comparable<Account>{
    private String branchOffice;
    private int id;

    public Account(String branchOffice, int id) {
        this.branchOffice = branchOffice.toLowerCase();
        this.id = id;
    }

    public String getBranchOffice() {
        return branchOffice;
    }

    public int getId() {
        return id;
    }

    @Override
    public int compareTo(Account o) {
        if(this.getId() < o.getId())
            return -1;
        if(this.getId() > o.getId())
            return 1;
        else
            return 0;
    }

    @Override
    public String toString(){
        return "El CBU es: "+id+"\n"+"Sucursal: "+branchOffice;
    }
}
