package main.Rojas;

import main.TP4.Queue.DynamicQueue;
import main.TP7.BinaryTree;
import main.TP8.SearchBinaryTree;

/**
 * Created by JoseRojas on 25/4/17.
 */
public class ReOrder {
    private DynamicQueue<Account> myQueue;
    private SearchBinaryTree<Account> aTree;
    private SearchBinaryTree<Account> bTree;

    public ReOrder(BinaryTree<Account> myBinaryTree){
        aTree=new SearchBinaryTree<>();
        bTree=new SearchBinaryTree<>();
        myQueue= new DynamicQueue<>();
        analyze(myBinaryTree);
        initialize();
    }

    private void analyze(BinaryTree<Account> tree){
        if (tree.isEmpty()){
            return;
        } else if (!(tree.hasLeft() && tree.hasRight())){
            myQueue.enqueue(tree.getRoot());
            return;
        } else if (tree.hasLeft() && !tree.hasRight()){
            myQueue.enqueue(tree.getRoot());
            analyze(tree.getLeft());
            return;
        } else if (tree.hasRight() && !tree.hasLeft()){
            myQueue.enqueue(tree.getRoot());
            analyze(tree.getRight());
            return;
        } else {
            myQueue.enqueue(tree.getRoot());
            analyze(tree.getLeft());
            analyze(tree.getRight());
            return;
        }
    }

    private void initialize(){
        while(!myQueue.isEmpty()){
            Account auxAccount = myQueue.dequeue();
            if(auxAccount.getBranchOffice().equals("a"))
                aTree.add(auxAccount);
            if(auxAccount.getBranchOffice().equals("b"))
                bTree.add(auxAccount);
        }
    }

    public SearchBinaryTree<Account> getATree() {
        return aTree;
    }

    public SearchBinaryTree<Account> getBTree() {
        return bTree;
    }

}
