package main.Rojas;

import main.TP7.BinaryTree;
import main.TP8.SearchBinaryTree;

/**
 * Created by JoseRojas on 25/4/17.
 */
public class ParcialTester {
    public static void main(String[] args) {
        Account account1= new Account("A", 1000001);
        Account account2= new Account("B", 1012301);
        Account account3= new Account("A", 1024001);
        Account account4= new Account("B", 1000025);
        Account account5= new Account("A", 1006476);
        Account account6= new Account("B", 1004564);
        Account account7= new Account("A", 1034524);
        Account account8= new Account("B", 1035635);
        Account account9= new Account("A", 1065756);
        Account account10= new Account("B", 1054651);
        Account account11= new Account("A", 1764015);
        Account account12= new Account("B", 1043701);
        Account account13= new Account("A", 1435341);
        Account account14= new Account("B", 1005361);
        Account account15= new Account("A", 1435431);


        BinaryTree<Account> aLL = new BinaryTree<>(account1);
        BinaryTree<Account> aLR = new BinaryTree<>(account2);
        BinaryTree<Account> aRL = new BinaryTree<>(account3);
        BinaryTree<Account> aRR = new BinaryTree<>(account4);
        BinaryTree<Account> bLL = new BinaryTree<>(account5);
        BinaryTree<Account> bLR = new BinaryTree<>(account6);
        BinaryTree<Account> bRL = new BinaryTree<>(account7);
        BinaryTree<Account> bRR = new BinaryTree<>(account8);
        BinaryTree<Account> aL = new BinaryTree<>(account9, aLL, aLR);
        BinaryTree<Account> aR = new BinaryTree<>(account10, aRL, aRR);
        BinaryTree<Account> bL = new BinaryTree<>(account11, bLL, bLR);
        BinaryTree<Account> bR = new BinaryTree<>(account12, bRL, bRR);
        BinaryTree<Account> a = new BinaryTree<>(account13, aL, aR);
        BinaryTree<Account> b = new BinaryTree<>(account14, bL, bR);
        BinaryTree<Account> testTree = new BinaryTree<>(account15, a, b);


        ReOrder reOrder= new ReOrder(testTree);
        SearchBinaryTree<Account> aTree= reOrder.getATree();
        SearchBinaryTree<Account> bTree= reOrder.getBTree();

        System.out.println("-------------------------------------------");
        System.out.println(aTree.search(account1).element);
        System.out.println("-------------------------------------------");
        System.out.println(bTree.search(account14).element);
        System.out.println("-------------------------------------------");
        System.out.println(aTree.getMin().element);
        System.out.println("-------------------------------------------");
        System.out.println(aTree.getMax().element);
        System.out.println("-------------------------------------------");
        System.out.println(bTree.getMin().element);
        System.out.println("-------------------------------------------");
        System.out.println(bTree.getMax().element);
        System.out.println("-------------------------------------------");

    }
}
