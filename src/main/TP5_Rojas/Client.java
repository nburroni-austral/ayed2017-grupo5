package main.TP5_Rojas;

/**
 * Created by JoseRojas on 19/4/17.
 */
public class Client {
    private int waitTime;

    public Client() {
        waitTime=0;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void addWaitTime() {
        waitTime++;
    }
}
