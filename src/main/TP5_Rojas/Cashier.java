package main.TP5_Rojas;

import main.TP4.Queue.DynamicQueue;
import main.ListTypes.List.StaticList;

/**
 * Created by JoseRojas on 19/4/17.
 */
public class Cashier {
    private int leisureTime;
    private StaticList<Client> clientsDay;
    private DynamicQueue<Client> queueClient;

    public Cashier(){
        leisureTime=0;
        clientsDay = new StaticList<>();
        queueClient = new DynamicQueue<>();
    }

    public void attend(){
        clientsDay.insertNext(queueClient.dequeue());
    }

    public void addLeisureTime() {
        leisureTime++;
    }

    public int getLeisureTime() {
        return leisureTime;
    }

    public StaticList<Client> getClientsDay() {
        return clientsDay;
    }

    public void addClient(Client client){
        queueClient.enqueue(client);
    }

    public boolean isAttend(){
        int rdm = (int) Math.floor(Math.random()*10 +1);
        if(rdm>0 && rdm <4)
            return false;
        else
            return true;
    }

    public boolean isEmptyQueue(){
        return queueClient.isEmpty();
    }

    public void addTimeClient(){
        DynamicQueue<Client> aux= new DynamicQueue<>();
        while(!queueClient.isEmpty()){
            Client auxClient= queueClient.dequeue();
            auxClient.addWaitTime();
            aux.enqueue(auxClient);
        }
        queueClient=aux;
    }

    public int getSizeQueue(){
        return queueClient.size();
    }
}
