package main.TP5_Rojas;

import main.ListTypes.List.StaticList;

/**
 * Created by JoseRojas on 19/4/17.
 */
public class Metrovias {
    private Cashier[] cashiers;

    public Metrovias(int lot){
        cashiers= new Cashier[lot];
        for(int i=0; i<cashiers.length;i++)
            cashiers[i]= new Cashier();
    }

    public void simulate(){
        for(int i=0; i<cashiers.length; i++){
            if(cashiers[i].isEmptyQueue())
                cashiers[i].addLeisureTime();
            if(!cashiers[i].isAttend())
                cashiers[i].attend();
            if(cashiers[i].isAttend())
                cashiers[i].addTimeClient();
        }
    }

    public void generateClients(){
        for(int i=0; i<5; i++){
            int random = (int) Math.floor(Math.random()*(cashiers.length));
            Client client = new Client();
            cashiers[random].addClient(client);
        }
    }

    public void theLast(){
        for(int i=0; i<cashiers.length; i++){
            Cashier cashier= cashiers[i];
            if(cashier.getSizeQueue()!=0){
                while(cashier.getSizeQueue()!=0)
                    cashier.attend();
            }
        }
    }

    public void closeDay(){
        System.out.println("-----------------------------------------------------");
        for(int i=0; i<cashiers.length; i++){
            StaticList<Client> aux= cashiers[i].getClientsDay();
            int auxSize= aux.size();
            int timeWait=0;
            for(int j=0; j<auxSize; j++){
                aux.goTo(j);
                Client auxClient= aux.getActual();
                if(auxClient!=null)
                    timeWait+=(auxClient.getWaitTime());
            }

            System.out.println("El tiempo esta representado en milisegundos.");
            System.out.println("Tiempo de ocio de esta caja: " + cashiers[i].getLeisureTime()*10000);
            System.out.println("Dinero recaudado: " + auxSize*0.7);
            System.out.println("Tiempo de espera promedio de esta caja: " + timeWait/auxSize*10000);
            System.out.println("-----------------------------------------------------");
        }
    }
}
