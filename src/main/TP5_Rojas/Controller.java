package main.TP5_Rojas;

import java.util.Scanner;

/**
 * Created by JoseRojas on 19/4/17.
 */
public class Controller {
    private Metrovias metrovias;

    public Controller(){
        Scanner scanner= new Scanner(System.in);
        System.out.println("Ingrese cantidad de cajas: ");
        int a = scanner.nextInt();
        metrovias=new Metrovias(a);
    }

    public void simulate(){
        int i=0;
        while(i!=5759){
            metrovias.generateClients();
            metrovias.simulate();
            i++;
        }
        metrovias.theLast();
        metrovias.closeDay();
    }
}
