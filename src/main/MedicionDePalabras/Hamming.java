package main.MedicionDePalabras;


public class Hamming {

    private static String word1;
    private static String word2;
    private static int changes = 0;

     /** Turns all the characters from both words into lowercase.
     * Invokes the toSize method.
     * Returns the number of differences between the words.
     */

    public static int inicialitation(String w1, String w2){
        word1=w1.toLowerCase();
        word2=w2.toLowerCase();
        toSize();
        return changes;
    }


    /** Compares both words character by character without taking into account the words' lengths.
     * If the words' lengths are different, it adds the difference between both words lengths to changes.
     */
    private static void toSize(){
        if((word2.length()>=word1.length())) {
            for(int i = 0; i < word1.length(); i++)
                if(word1.charAt(i) != word2.charAt(i))
                    changes++;
            changes = changes + (word2.length() - word1.length());
        }

        if(word1.length()>word2.length()) {
            for(int i = 0; i < word2.length(); i++)
                if(word2.charAt(i) != word1.charAt(i))
                    changes++;
            changes = changes + (-word2.length() + word1.length());
        }
    }
}