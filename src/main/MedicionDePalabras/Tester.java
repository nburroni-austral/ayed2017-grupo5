package main.MedicionDePalabras;

/**
 * Created by JoseRojas on 17/3/17.
 */
public class Tester {
    public static void main(String[] args){
        System.out.println(Hamming.inicialitation("Hello", "World!"));
        System.out.println(Levenshtein.inicialitation("Administration", "Nistra"));
    }
}
