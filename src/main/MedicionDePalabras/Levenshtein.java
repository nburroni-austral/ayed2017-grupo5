package main.MedicionDePalabras;

import java.util.Arrays;


public class Levenshtein {

    private static String word1;
    private static String word2;
    private static int[][] matrix;
    private static int[] minimumValue;


    /**First the method turns all the characters into lowercase so that there are no differences between both words.
     * It sets the matrix's size and invokes the fill method.
     * Ultimately, it returns the matrix's last number.
     */
    public static int inicialitation(String w1, String w2){
        word1 = w1.toLowerCase();
        word2 = w2.toLowerCase();
        matrix = new int[word1.length()+1][word2.length()+1];

        fill();
        return matrix[word1.length()][word2.length()];
    }


    /** It fills the matrix following a mathematical algorithm so that the last position is the lowest
     * number of changes a word needs to go through to turn into another one.
     *
     * Algorithm:
     *
     * It creates a matrix with the first word's characters as columns and the second word ones as rows.
     * Starting in any position [i][j] of the matrix (i,j > 0), the algorithm looks for the lowest value
     * in any of the previous positions ([i-1][j]  [i][j-1]  [i-1][j-1]). If the letter from column i
     * is the same as the one from row j, it fills the position [i][j] with the lowest value previously
     * mentioned. If they are not the same, the position will be filled with the lowest number plus 1.
     */
    private static void fill(){
        for(int i = 1; i<(word1.length()+1); i++)
            for (int j = 1; j<(word2.length()+1); j++){
                minimumValue = new int[3];
                matrix[0][0] = 0;
                matrix[i][0] = i;
                matrix[0][j] = j;

                minimumValue[0] = matrix[i][j-1];
                minimumValue[1] = matrix[i-1][j];
                minimumValue[2] = matrix[i-1][j-1];
                Arrays.sort(minimumValue);

                matrix[i][j] = minimumValue[0] + ( (word1.charAt(i-1) == word2.charAt(j-1)) ? 0 : 1 );
            }
    }
}