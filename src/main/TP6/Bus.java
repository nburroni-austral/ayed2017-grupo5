package main.TP6;

/**
 * Created by JoseRojas on 16/5/17.
 */
public class Bus implements Comparable<Bus>{
    private int lineNumber;
    private int internalNumber;
    private int quantitySeats;
    private boolean disabled;

    public Bus(int lineNumber, int internalNumber, int quantitySeats, boolean disabled) {
        this.lineNumber = lineNumber;
        this.internalNumber = internalNumber;
        this.quantitySeats = quantitySeats;
        this.disabled = disabled;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public int getInternalNumber() {
        return internalNumber;
    }

    public void setInternalNumber(int internalNumber) {
        this.internalNumber = internalNumber;
    }

    public int getQuantitySeats() {
        return quantitySeats;
    }

    public void setQuantitySeats(int quantitySeats) {
        this.quantitySeats = quantitySeats;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    @Override
    public String toString() {
        return ("Nro de linea: "+lineNumber+"        Nro interno: "+internalNumber+"        cantidad de asientos: "+quantitySeats+"        Apto para discapacitados: "+disabled);
    }

    @Override
    public int compareTo(Bus o) {
        if(lineNumber<o.getLineNumber())
            return -1;
        if(lineNumber>o.getLineNumber())
            return 1;
        else
            return compareInternalNumber(o);
    }

    private int compareInternalNumber(Bus o){
        if(internalNumber<o.getInternalNumber())
            return -1;
        if(internalNumber>o.getInternalNumber())
            return 1;
        else
            return compareQuantitySeats(o);
    }

    private int compareQuantitySeats(Bus o){
        if(quantitySeats<o.getQuantitySeats())
            return -1;
        if(quantitySeats>o.getQuantitySeats())
            return 1;
        else
            return 0;
    }

}
