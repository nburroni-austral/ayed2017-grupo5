package main.TP6;

import java.util.Scanner;

/**
 * Created by JoseRojas on 16/5/17.
 */
public class TransportTester {
    public static void main(String[] args) {
        TransportSystem transportSystem= new TransportSystem();
        Scanner scanner= new Scanner(System.in);
        boolean isOn= true;
        while(isOn!=false){
            System.out.println("Digite: \n       1 para agregar colectivo \n       2 para remover \n       3 para un informe completo \n       4 para cantidad de colectivos aptos \n       5 para cantidad de colectivos con minimo x asientos \n       6 para guardar \n       7 para recuperar \n       0 para salir");
            int choose=scanner.nextInt();
            switch (choose){
                case 0:
                    isOn=false;
                    break;
                case 1:
                    System.out.println("Primero ingrese numero de linea, luego numero interno, luego cantidad de asientos y por ultimo true o false si esta apto para discapacitados");
                    Bus bus= new Bus(scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextBoolean());
                    transportSystem.add(bus);
                    break;
                case 2:
                    System.out.println("Primero numero de linea y luego el interno");
                    transportSystem.remove(scanner.nextInt(), scanner.nextInt());
                    break;
                case 3:
                    System.out.println(transportSystem);
                    break;
                case 4:
                    System.out.println("Cantidad de colectivos aptos: "+transportSystem.quantityDisable());
                    break;
                case 5:
                    System.out.println("Ingrese cantidad de asientos");
                    System.out.println("Cantidad de colectivos: "+transportSystem.quantityWithSeats(scanner.nextInt()));
                    break;
                case 6:
                    transportSystem.saveListInDisk();
                    break;
                case 7:
                    transportSystem.setMyList(transportSystem.readListFromDisk());
                    break;
            }
        }
    }
}
