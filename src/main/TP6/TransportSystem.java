package main.TP6;

import main.ListTypes.SortedList.StaticSortedList;
import struct.istruct.list.SortedList;

import java.io.*;

/**
 * Created by JoseRojas on 16/5/17.
 */
public class TransportSystem {
    private SortedList<Bus> myList;

    public TransportSystem(){
        myList= new StaticSortedList<>(Bus.class);
    }

    public void add(Bus b){
        myList.insert(b);
    }

    public void remove(int lineNumber, int interalNumber){
        for(int i=0; i<myList.size(); i++){
            myList.goTo(i);
            if(myList.getActual().getLineNumber()==lineNumber && myList.getActual().getInternalNumber()==interalNumber)
                myList.remove();
        }
    }

    @Override
    public String toString() {
        String result="";
        for(int i=0; i<myList.size(); i++) {
            myList.goTo(i);
            result += myList.getActual().toString() + "\n" + "--------------------------------------------------------------------------------------------------------------------" + "\n";
        }
        return result;
    }

    public int quantityDisable(){
        int result=0;
        for(int i=0; i<myList.size(); i++) {
            myList.goTo(i);
            if (myList.getActual().isDisabled())
                result++;
        }
        return result;
    }

    public int quantityWithSeats(int seats){
        int result=0;
        for(int i=0; i<myList.size(); i++) {
            myList.goTo(i);
            if (myList.getActual().getQuantitySeats() >= seats)
                result++;
        }
        return result;
    }

    public void saveListInDisk(){
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;
        try {
            fout = new FileOutputStream("c:\\temp\\address.ser");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(myList);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public StaticSortedList<Bus> readListFromDisk(){
        FileInputStream fin = null;
        ObjectInputStream ois = null;
        StaticSortedList<Bus> result = null;
        try {
            fin = new FileInputStream("c:\\temp\\address.ser");
            ois = new ObjectInputStream(fin);
            result = (StaticSortedList<Bus>) ois.readObject();
        } catch (Exception ex){
            ex.printStackTrace();
        } finally {
            if (fin != null){
                try {
                    fin.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
            if (ois != null){
                try {
                    ois.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    public void setMyList(SortedList<Bus> myList) {
        this.myList = myList;
    }
}
