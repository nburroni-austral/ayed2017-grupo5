package main.TPTrees.RedBlackTree;


public class RedBlackTree<T extends Comparable<T>> {

    private RedBlackNode<T> root;

    public RedBlackTree() {
        root = null;
    }

    public RedBlackNode<T> getRoot() {
        return root;
    }

    public void insert(T data) {
        root = insert(root, data);
        root.turnBlack();
    }


    private RedBlackNode<T> insert(RedBlackNode<T> node, T data) {
        if (node == null) {
            RedBlackNode<T> newNode = new RedBlackNode<T>(data);
            return newNode;
        }

        int comparison = data.compareTo(node.getData());

        if (comparison < 0) {
            node.setLeftChild(insert(node.getLeftChild(), data));
        } else if (comparison > 0) {
            node.setRightChild(insert(node.getRightChild(), data));
        } else {
            return node;
        }


        if (isRed(node.getLeftChild()) && isRed(node.getRightChild())) {
            if (node != root) {
                node.turnRed();
            }
            node.getRightChild().turnBlack();
            node.getLeftChild().turnRed();
        } else if (isRed(node.getLeftChild()) && isRed(node.getLeftChild().getLeftChild())) {
            node.turnRed();
            node.getLeftChild().turnBlack();
            node = rightRotation(node);
        } else if (isRed(node.getRightChild()) && isRed(node.getRightChild().getRightChild())) {
            node.turnRed();
            node.getRightChild().turnBlack();
            node = leftRotation(node);
        } else if (isRed(node.getLeftChild()) && isRed(node.getLeftChild().getRightChild())) {
            node.turnRed();
            node.getLeftChild().getRightChild().turnBlack();
            node.setLeftChild(leftRotation(node.getLeftChild()));
            node = rightRotation(node);
        } else if (isRed(node.getRightChild()) && isRed(node.getRightChild().getLeftChild())) {
            node.turnRed();
            node.getRightChild().getLeftChild().turnBlack();
            node.setRightChild(rightRotation(node.getRightChild()));
            node = leftRotation(node);
        }
        return node;
    }


    public boolean isRed(RedBlackNode<T> node) {
        if (node == null) {
            return false;
        }
        return node.isRed();
    }


    private RedBlackNode<T> rightRotation(RedBlackNode<T> grandparent) {
        RedBlackNode<T> parent = grandparent.getLeftChild();
        RedBlackNode<T> rightChildOfParent = parent.getRightChild();
        parent.setRightChild(grandparent);
        grandparent.setLeftChild(rightChildOfParent);
        return parent;
    }


    private RedBlackNode<T> leftRotation(RedBlackNode<T> grandparent) {
        RedBlackNode<T> parent = grandparent.getRightChild();
        RedBlackNode<T> leftChildOfParent = parent.getLeftChild();
        parent.setLeftChild(grandparent);
        grandparent.setRightChild(leftChildOfParent);
        return parent;
    }
}