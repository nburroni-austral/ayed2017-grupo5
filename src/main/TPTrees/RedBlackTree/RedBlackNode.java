package main.TPTrees.RedBlackTree;


public class RedBlackNode<T extends Comparable<T>> {

    private T data;
    private boolean color;
    private RedBlackNode<T> rightChild;
    private RedBlackNode<T> leftChild;
    private boolean empty;


    public RedBlackNode(T data) {
        this.data = data;
        color = true;
        rightChild = null;
        leftChild = null;
        empty = false;
    }

    public void setData(T data) {
        this.data = data;
    }

    public void turnRed() {
        color = true;
    }

    public void turnBlack() {
        color = false;
    }

    public void setLeftChild(RedBlackNode<T> node) {
        leftChild = node;
    }

    public void setRightChild(RedBlackNode<T> node) {
        rightChild = node;
    }

    public void empty() {
        empty = true;
    }

    public T getData() {
        return data;
    }

    public boolean isRed() {
        return color;
    }

    public RedBlackNode<T> getLeftChild() {
        return leftChild;
    }

    public RedBlackNode<T> getRightChild() {
        return rightChild;
    }

    public boolean isEmpty() {
        return empty;
    }
}