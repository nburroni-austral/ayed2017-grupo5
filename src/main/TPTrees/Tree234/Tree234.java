package main.TPTrees.Tree234;

import main.TPTrees.Tree234.Nodes.*;

/**
 * Created by JoseRojas on 24/5/17.
 */
public class Tree234<T extends Comparable> {
    private Node<T> root;
    private Class<T> tClass;

    public Tree234(Class<T> tClass){
        this.tClass=tClass;
        root= new Node2<>(tClass);
    }

    public Tree234(Class<T> tClass, Node<T> root){
        this.tClass=tClass;
        this.root=root;
    }

    public void insert(T t){
        root = root.findNode(t);
        root = root.insert(t);
        Node<T> father = root.getFather();
        if(father!=null){
            while(father.getFather() != null){
                father = father.getFather();
            }
            root = father;
        }
        reOrder(root);
    }

    public void print(){
        root.print();
    }

    private void reOrder(Node<T> root){
        if( (!root.isLeaf() && root.getType()==4) || (root.isRoot() && root.getType()==4) ) {
            Node4<T> aux = (Node4<T>) root;
            root=aux.destroy();
        }
    }
}
