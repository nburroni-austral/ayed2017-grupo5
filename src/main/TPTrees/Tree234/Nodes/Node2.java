package main.TPTrees.Tree234.Nodes;

import java.lang.reflect.Array;

public class Node2<T extends Comparable> extends Node<T>{
    T data;
    int type;
    private Node left;
    private Node right;

    public Node2(Class<T> tclass1){
        type=2;
        tClass=tclass1;
    }

    @Override
    public Node search(T t) {
        int comparedWithData = t.compareTo(data);
        if(this.isLeaf())
            return this;
        else{
            if(comparedWithData>0){
                return right.search(t);
            }else{
                return left.search(t);
            }
        }
    }

    @Override
    public boolean isLeaf() {
        return (left ==null && right == null);
    }

    @Override
    public Node insert(T t) {
        if(data==null)
            data=t;
        else
            return convetTo3(t);
        return this;
    }

    @Override
    public void setChild(T t, Node child) {
        if(t.compareTo(data)>0)
            this.setRight(child);
        else
            this.setLeft(child);
    }

    @Override
    public void print() {
        System.out.println("("+data+")");
        if(left!=null)
            left.print();
        if(right!=null)
            right.print();
    }

    @Override
    public T[] getData() {
        T[] result = (T[]) Array.newInstance(tClass, type-1);
        result[0]=data;
        return result;
    }

    private Node3 convetTo3(T t){
        Node3 result = new Node3(tClass);
        result.setFather(getFather());
        result.setLeft(left);
        result.setRight(right);

        if(data.compareTo(t)>0){
            result.data2=data;
            result.data=t;
        }
        else{
            result.data=data;
            result.data2=t;
        }

        if(getFather()!=null)
            getFather().setChild(t, result);

        return result;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
        if(left!=null){
            left.setFather(this);
        }
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
        if(right!=null){
            right.setFather(this);
        }
    }

    @Override
    public Node<T> findNode(T t) {
        if(isLeaf())
            return this;
        if (t.compareTo(data) < 0) {
            return left.findNode(t);
        }
        else {
            return right.findNode(t);
        }
    }
}
