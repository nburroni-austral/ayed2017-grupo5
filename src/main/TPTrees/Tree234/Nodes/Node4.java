package main.TPTrees.Tree234.Nodes;

import java.lang.reflect.Array;

public class Node4<T extends Comparable> extends Node3<T>{
    T data3;
    Node center2;

    public Node4(Class<T> tClass1){
        super(tClass1);
        type=4;
    }

    @Override
    public Node search(T t) {
        Node node = destroy();
        return node.search(t);
    }


    @Override
    public Node insert(T t) {
        setFather(destroy());
        Node2 result = (Node2) getFather();
        if(t.compareTo(data2)<0)
            result.getLeft().insert(t);
        else
            result.getRight().insert(t);
        return result;
    }

    @Override
    public void setChild(T t, Node child) {
        if(t.compareTo(data)<0)
            setLeft(child);
        else{
            if(t.compareTo(data2)<0)
                setCenter(child);
            else{
                if(t.compareTo(data3)<0)
                    setCenter2(child);
                else
                    setRight(child);
            }
        }
    }

    @Override
    public void print() {
        System.out.println("("+data + ", "+data2+ ", "+data3+")");
        if(getLeft()!=null)
            getLeft().print();
        if(getCenter()!=null)
            getCenter().print();
        if(center2!=null)
            center2.print();
        if(getRight()!=null)
            getRight().print();
    }

    @Override
    public T[] getData() {
        T[] result = (T[]) Array.newInstance(tClass, type-1);
        result[0] = data;
        result[1] = data2;
        result[2] = data3;
        return result;
    }

    public Node getCenter2() {
        return center2;
    }

    public void setCenter2(Node center2) {
        this.center2 = center2;
        if(center2!=null){
            center2.setFather(this);
        }
    }

    public Node destroy() {
        if(getFather()==null){
            setFather(new Node2(tClass));
        }
        setFather(getFather().insert(data2));

        Node2 nodeR = new Node2(tClass);
        nodeR.data = data3;
        nodeR.setFather(getFather());
        nodeR.setLeft(center2);
        nodeR.setRight(getRight());

        Node2 nodeL = new Node2(tClass);
        nodeL.data = data;
        nodeL.setFather(getFather());
        nodeL.setLeft(getLeft());
        nodeL.setRight(getCenter());

        getFather().setChild(nodeR.data,nodeR);
        getFather().setChild(nodeL.data,nodeL);

        return getFather();
    }

    @Override
    public Node<T> findNode(T t) {
        if(isLeaf())
            return this;
        else {
            if (t.compareTo(data) < 0) {
                return getLeft().findNode(t);
            }
            if ((t.compareTo(data) > 0) && (t.compareTo(data2) < 0)){
                return getCenter().findNode(t);
            }
            if ((t.compareTo(data2) > 0) && (t.compareTo(data3) < 0)){
                return getCenter2().findNode(t);
            }
            else {
                return getRight().findNode(t);
            }
        }
    }
}
