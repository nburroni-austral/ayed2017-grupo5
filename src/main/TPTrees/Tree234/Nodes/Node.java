package main.TPTrees.Tree234.Nodes;

/**
 * Created by JoseRojas on 24/5/17.
 */
public abstract class Node<T extends Comparable> {
    private Node father;
    Class<T> tClass;
    int type;
    public abstract Node search(T t);
    public abstract boolean isLeaf();
    public abstract Node insert(T t);
    public abstract void setChild(T t,Node child);
    public abstract void print();
    public abstract Node<T> findNode(T t);
    public Node getFather() {
        return father;
    }
    public abstract T[] getData();
    public void setFather(Node father) {
        this.father = father;
    }
    public int getType(){
        return type;
    }
    public boolean isRoot(){
        return getFather()==null;
    }
}
