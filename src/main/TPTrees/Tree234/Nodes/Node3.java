package main.TPTrees.Tree234.Nodes;

import java.lang.reflect.Array;

public class Node3<T extends Comparable> extends Node2<T>{
    T data2;
    private Node center;

    public Node3(Class<T> tClass1){
        super(tClass1);
        type=3;
    }

    @Override
    public Node search(T t) {
        if(this.isLeaf())
            return this;
        else{
            if(t.compareTo(data)<0)
                return getLeft().search(t);
            else{
                if(t.compareTo(data2)<0)
                    return getCenter().search(t);
                else
                    return getRight().search(t);
            }
        }
    }

    @Override
    public Node insert(T t) {
        return converTo4(t);
    }

    @Override
    public void setChild(T t, Node child) {
        if(t.compareTo(data)<0)
            setLeft(child);
        else{
            if(t.compareTo(data2)<0)
                setCenter(child);
            else{
                setRight(child);
            }
        }
    }

    @Override
    public void print() {
        System.out.println("("+data + ", "+data2+")");
        if(getLeft()!=null)
            getLeft().print();
        if(center!=null)
            center.print();
        if(getRight()!=null)
            getRight().print();
    }

    @Override
    public T[] getData() {
        T[] result = (T[]) Array.newInstance(tClass, type-1);
        result[0] = data;
        result[1] = data2;
        return result;
    }

    public Node getCenter() {
        return center;
    }

    public void setCenter(Node center) {
        this.center = center;
        if(center!=null){
            center.setFather(this);
        }
    }

    private Node4 converTo4(T t){
        Node4 result = new Node4(tClass);
        T[] auxArray = sort(t);

        result.data=auxArray[0];
        result.data2=auxArray[1];
        result.data3=auxArray[2];

        result.setFather(getFather());
        result.setLeft(getLeft());
        result.setRight(getRight());
        result.setCenter(getCenter());

        if(getFather()!=null)
            getFather().setChild(t, result);

        return result;
    }

    private T[] sort(T t){
        T[] result = getData(t);

        for(int i=0; i<(result.length-1);i++){
            for(int j=i+1; j<result.length; j++){
                if(result[i].compareTo(result[j])>0){
                    T aux= result[i];
                    result[i]=result[j];
                    result[j]=aux;
                }
            }
        }

        return result;
    }

    private T[] getData(T t){
        T[] result = (T[]) Array.newInstance(tClass, type);
        result[0] = data;
        result[1] = data2;
        result[2] = t;
        return result;
    }

    @Override
    public Node<T> findNode(T t) {
        if(isLeaf())
            return this;
        else {
            if (t.compareTo(data) < 0) {
                return getLeft().findNode(t);
            }
            if ((t.compareTo(data) > 0) && (t.compareTo(data2) < 0)){
                return getCenter().findNode(t);
            }
            else {
                return getRight().findNode(t);
            }
        }
    }
}
