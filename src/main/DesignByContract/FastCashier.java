package main.DesignByContract;


import java.util.ArrayList;

public class FastCashier {

    public static int maxNumberOfItems = 10;
    private ArrayList<Item> itemsToCash;
    private int numberOfItemsToCash;
    /*@ invariant numberOfItemsToCash > 0 && numberOfItemsToCash <= maxNumberOfItems;
    @*/

    public FastCashier(/*@ non_null @*/ ArrayList<Item> itemsToCash){
        this.itemsToCash = itemsToCash;
        numberOfItemsToCash = itemsToCash.size();
    }

    /*@ assert (\forall int i; 0 <= i && i < numberOfItemsToCash; itemsToCash(i).getPrice > null);
    @*/
    public /*@ pure @*/ double cashItems(){
        double costOfItems = 0;
        for(Item itemToCash : itemsToCash){
            costOfItems += itemToCash.getPrice();
        }
        return costOfItems;
    }

    /*@ requires discount > 0;
    ensures \result < cashItems;
    @*/
    public /*@ pure @*/ double cashItemsWithDiscount(int discount){
        return cashItems() * (100 - discount) / 100;
    }


    /*@ requires numberOfItemsToCash > 1;
    assert (\exists int i; 0 <= i && i < numberOfItemsToCash; itemsToCash(i).equals(itemToRemove));
    ensures- itemsToCash.size() == \old(itemsToCash).size() + 1;
    @*/
    public void removeItem(/*@ non_null @*/ Item itemToRemove){
        itemsToCash.remove(itemToRemove);
        numberOfItemsToCash--;
    }


    public void addItem(/*@ non_null @*/ Item itemToAdd){
        itemsToCash.add(itemToAdd);
        numberOfItemsToCash++;
    }

    /*@
    signals (TooManyItemsException() e)
        itemsToAdd.size() + numberOfItemsToCash > maxNumberOfItems;
    @*/
    public void addItems(/*@ non_null @*/ ArrayList<Item> itemsToAdd) {
        if (itemsToAdd.size() + numberOfItemsToCash <= maxNumberOfItems) {
            for (Item itemToAdd : itemsToAdd) {
                addItem(itemToAdd);
            }
        } else {
            throw new TooManyItemsException();
        }
    }


}
