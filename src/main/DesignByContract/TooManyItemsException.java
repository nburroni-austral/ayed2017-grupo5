package main.DesignByContract;


public class TooManyItemsException extends RuntimeException{

    public TooManyItemsException(){
        super("You have exceeded the maximum number of items for this cashier");
    }
}
