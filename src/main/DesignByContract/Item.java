package main.DesignByContract;


public class Item {

    private String name;
    private double price;

    public Item(String name, double price){
        this.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }



    /*@ assignable price
    @*/
    public void changePrice(double price) {
        this.price = price;
    }
}
