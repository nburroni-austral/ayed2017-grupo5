package main.HorseMovements.Model;

import struct.istruct.Stack;

/** Added to make the .jar
 */

public class StaticStack<T> implements Stack<T>{
    private int size=0;
    private Object[] stack;

    public StaticStack(){
        stack = new Object[50];
    }



    @Override
    public void push(T o) {
        if(size == stack.length)
            grow();
        size++;
        stack[size-1]= o;
    }



    @Override
    public void pop() {
        size--;
        if(size == -1)
            size=0;
    }


    @Override
    public T peek() {
        if(size==0)
            return null;
        return (T) stack[size-1];
    }


    @Override
    public boolean isEmpty() {
        return ( (size==0) ? true : false );
    }


    @Override
    public int size() {
        return size;
    }


    @Override
    public void empty() {
        size=0;
    }


    private void grow(){
        Object[] stack2 = new Object[2*stack.length];
        for (int i =0; i<stack.length;i++){
            stack2[i] = stack[i];
        }
        stack = stack2;
    }
}
