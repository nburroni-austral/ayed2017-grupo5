package main.HorseMovements.Model;


public class HorseSystem {

    private ChessBoard myBoard;
    private int size;
    public StaticStack<Location>[] myStacks;
    private int index = 0;
    private boolean init= true;
    public boolean finish= false;
    private Location previous;

    public HorseSystem(Location start, int jumps) {
        size=jumps+1;
        previous =start;
        myStacks = new StaticStack[size];
        myBoard= new ChessBoard();
        for(int i =0; i<myStacks.length; i++)
            myStacks[i] = new StaticStack<Location>();
    }

    // Initializes myBoard and pushes the location set by the user into the stack.
    private void inicialitation(Location start){
        myStacks[0].push(start);
        myBoard.inicialitation(start);

    }

    // Adds the possible jumps to the stack.
    private void possibleJump(){
        Location position = myBoard.getHorseLocation();
        Location position1=position.add(2,1);
        Location position2=position.add(2,-1);
        Location position3=position.add(1,2);
        Location position4=position.add(1,-2);
        Location position5=position.add(-1,2);
        Location position6=position.add(-1,-2);
        Location position7=position.add(-2,1);
        Location position8=position.add(-2,-1);

        if( !previous.equals(position1) && (position1.belong()) )
            myStacks[index + 1].push(position1);
        if( !previous.equals(position2) && (position2.belong()) )
            myStacks[index + 1].push(position2);
        if( !previous.equals(position3) && (position3.belong()) )
            myStacks[index + 1].push(position3);
        if( !previous.equals(position4) && (position4.belong()) )
            myStacks[index + 1].push(position4);
        if( !previous.equals(position5) && (position5.belong()) )
            myStacks[index + 1].push(position5);
        if( !previous.equals(position6) && (position6.belong()) )
            myStacks[index + 1].push(position6);
        if( !previous.equals(position7) && (position7.belong()) )
            myStacks[index + 1].push(position7);
        if( !previous.equals(position8) && (position8.belong()) )
            myStacks[index + 1].push(position8);
        index++;
        previous=position;
    }

    private void nextSystem(int in){
        if(in==0 && init) {
            inicialitation(previous);
        }
        if(in>=0 && in<myStacks.length-1 && myStacks[in+1].isEmpty() && !init && !myStacks[in].isEmpty()) {
            myBoard.move(myStacks[in].peek());
            possibleJump();
        }
        if(in==myStacks.length-1 && !myStacks[in].isEmpty())
            myStacks[in].pop();
        if(in!=0 && myStacks[in].isEmpty()){
            index--;
            in--;
            myStacks[in].pop();
        }
        if(myStacks[in].isEmpty() && in!=0) {
            while (myStacks[in-1].size()!=1 && in==0){
                index--;
                in--;
                myStacks[in].pop();
            }
        }
        if(in-1==0 && myStacks[in].isEmpty())
            finish=true;
        init=false;
    }

    public void next(){
        int n = index;
        nextSystem(n);
    }
}
