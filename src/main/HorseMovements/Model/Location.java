package main.HorseMovements.Model;


public class Location {
    private int horizontalNumber;
    private int verticalNumber;

    public Location(int horizontalNumber, int verticalNumber) {
        this.horizontalNumber = horizontalNumber;
        this.verticalNumber = verticalNumber;
    }


    public int getHorizontalNumber() {
        return horizontalNumber;
    }

    public void setHorizontalNumber(int horizontalNumber) {
        this.horizontalNumber = horizontalNumber;
    }

    public int getVerticalNumber() {
        return verticalNumber;
    }

    public void setVerticalNumber(int verticalNumber) {
        this.verticalNumber = verticalNumber;
    }

    /** Sets both numbers to set a new location.
     */
    public void setNumber(int hN, int vN){
        setHorizontalNumber(hN);
        setVerticalNumber(vN);
    }

    /** Checks if both locations are the same.
     */
    public boolean equals(Location a){
        boolean result = false;
        if( (this.getVerticalNumber() == a.getVerticalNumber()) && (this.getHorizontalNumber() == a.getHorizontalNumber()) )
            result=true;
        return result;
    }

    /** Modifies the original location by adding an horizontal and vertical component.
     */
    public Location add(int horizontal, int vertical) {
        int h= getHorizontalNumber() + horizontal;
        int v= getVerticalNumber()+vertical;
        Location result=new Location(h,v);
        return result;
    }

    /** Checks if the current location belongs to the board.
     */
    public boolean belong(){
        boolean result = false;
        if( (this.getHorizontalNumber() >= 0) && (this.getHorizontalNumber()<8) && (this.getVerticalNumber()>=0) && (this.getVerticalNumber()<8) )
            result = true;
        return result;
    }

    public String toString(){
        return "("+horizontalNumber+";"+verticalNumber+")";
    }
}
