package main.HorseMovements.Model;


public class ChessBoard {
    private int[][] board = new int[8][8];

    public ChessBoard(){
        for(int i =0; i<8; i++)
            for(int j =0; j<8; j++)
                board[i][j]=0;
    }

    /** Returns true if this cell is empty.
     */
    public boolean isEmptyLocation(Location a){
        return ((board[a.getHorizontalNumber()][a.getVerticalNumber()] == 0) ? true : false);
    }


    /** Initializes the board full of 0s and a 1 in the starting location.
     */
    public void inicialitation(Location a){
        board[a.getHorizontalNumber()][a.getVerticalNumber()]=1;
    }


    /** Returns the horse's location.
     */
    public Location getHorseLocation(){
        Location location = new Location(-1,-1);
        for(int i =0; i<8; i++)
            for(int j =0; j<8; j++)
                if(board[i][j] == 1)
                    location.setNumber(i,j);
        return location;
    }


    /** Moves the horse to a new location.
     */
    public void move(Location newLocation){
        Location pastLocation = getHorseLocation();
        board[pastLocation.getHorizontalNumber()][pastLocation.getVerticalNumber()]= 0;
        board[newLocation.getHorizontalNumber()][newLocation.getVerticalNumber()]= 1;
    }
}
