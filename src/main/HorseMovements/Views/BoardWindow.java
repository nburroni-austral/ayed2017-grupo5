package main.HorseMovements.Views;

import main.HorseMovements.Model.Location;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;


public class BoardWindow extends JFrame {
    public JButton[][] myButtons= new JButton[8][8];
    public JButton nextButton;
    private ActionListener ac;
    private JPanel boardPanel;

    ImageIcon horse= new ImageIcon(getClass().getResource("resources/images.png"));
    ImageIcon white= new ImageIcon(getClass().getResource("resources/blanco.png"));
    ImageIcon black= new ImageIcon(getClass().getResource("resources/negro.jpg"));

    private JLabel[] myLabels;


    public int k=0;

    /** Creates a window for the chessboard with its configuration.
     */
    public BoardWindow(ActionListener ac, int sizeArrayLabels){
        this.ac=ac;
        myLabels= new JLabel[sizeArrayLabels+1];
        setTitle("Horse Movement");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(680, 700);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);

        Image horseImg = horse.getImage().getScaledInstance( 50, 50,  java.awt.Image.SCALE_SMOOTH ) ;
        horse = new ImageIcon(horseImg);

        Image whiteImg = white.getImage().getScaledInstance( 60, 60,  java.awt.Image.SCALE_SMOOTH ) ;
        white = new ImageIcon(whiteImg);

        Image blackImg = black.getImage().getScaledInstance( 60, 60,  java.awt.Image.SCALE_SMOOTH ) ;
        black = new ImageIcon(blackImg);

        JPanel theWindowPanel= new JPanel();
        JPanel thePanel= new JPanel();
        JPanel theNextPanel= new JPanel();
        boardPanel= new JPanel();
        JPanel locationPanel= new JPanel();

        theWindowPanel.setLayout(new BoxLayout(theWindowPanel, BoxLayout.X_AXIS));
        thePanel.setLayout(new BoxLayout(thePanel, BoxLayout.Y_AXIS));
        theNextPanel.setLayout(new BoxLayout(theNextPanel, BoxLayout.X_AXIS));
        boardPanel.setLayout(new GridLayout(8,8));
        locationPanel.setLayout(new BoxLayout(locationPanel, BoxLayout.X_AXIS));

        boardPanel.setBackground(new Color(0, 0, 0));

        add(theWindowPanel);
        theWindowPanel.add(thePanel);
        theWindowPanel.add(theNextPanel);
        thePanel.add(locationPanel);
        thePanel.add(boardPanel);

        for(int i=0; i<8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 0) {
                    myButtons[i][j] = new JButton();
                    myButtons[i][j].setIcon(white);
                    myButtons[i][j].addActionListener(ac);
                } else {
                    myButtons[i][j] = new JButton();
                    myButtons[i][j].setIcon(black);
                    myButtons[i][j].setBorder(new TitledBorder(""));
                    myButtons[i][j].addActionListener(ac);
                }
                boardPanel.add(myButtons[i][j]);
            }
        }

        for(int i=0; i<sizeArrayLabels+1; i++){
            myLabels[i]= new JLabel();
            //myLabels[i].
            locationPanel.add(myLabels[i]);
        }

        nextButton = new JButton("Next");
        nextButton.addActionListener(ac);
        theNextPanel.add(nextButton);

    }

    /** Changes the horse's location in the window's board.
     */
    public void setHorse(Location location){
        boardPanel.removeAll();
        int h = location.getHorizontalNumber();
        int v= location.getVerticalNumber();
        for(int i=0; i<8; i++) {
            for (int j = 0; j < 8; j++) {
                if (h == i && v == j) {
                    myButtons[i][j] = new JButton();
                    myButtons[i][j].setIcon(horse);
                } else if ((i + j) % 2 == 0) {
                    myButtons[i][j] = new JButton();
                    myButtons[i][j].setIcon(white);
                } else {
                    myButtons[i][j] = new JButton();
                    myButtons[i][j].setIcon(black);
                    myButtons[i][j].setBorder(new TitledBorder(""));
                }
                boardPanel.add(myButtons[i][j]);
            }
        }
        SwingUtilities.updateComponentTreeUI(this);
    }

    /** Takes care of showing in which location of the board the horse is.
     */
    public void setStringLabels(String str){
        myLabels[k].setText(" | " + str + " | ");
        k++;
    }


    /** Refreshes the window to show the horse's movement from its last location without showing it last
     */
    public void refresh(){
        repaint();
    }
}
