package main.HorseMovements.Controller;

import main.HorseMovements.Views.BoardWindow;
import main.HorseMovements.Model.HorseSystem;
import main.HorseMovements.Model.Location;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller implements ActionListener {
    BoardWindow boardWindow;
    HorseSystem horseSystem;
    private int index =0;
    private int numberJumps;

    public Controller(){
        getJumpsUser();
        boardWindow= new BoardWindow(this, numberJumps);
    }

    private void inicialitation(Location location){
        horseSystem= new HorseSystem(location, numberJumps);
    }

    /** Checks which button is clicked and performs the corresponding action.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object buttonSelected= e.getSource();
        for(int i=0; i<8; i++)
            for(int j=0; j<8; j++)
                if(buttonSelected.equals(boardWindow.myButtons[i][j]) && index<1) {
                    inicialitation(new Location(i,j));
                    index++;
                }
        if(buttonSelected.equals(boardWindow.nextButton)) {
            horseSystem.next();
            refresh();
        }
   }

   // Refreshes the board to show the horse's new position.
   private void refresh(){
        int a=horseSystem.myStacks.length-1;
        if(horseSystem.finish)
            JOptionPane.showMessageDialog(null,"Se completo todos los posible movimientos del caballo a partir de esa posicion");
        for(int i =horseSystem.myStacks.length-1; i>-1; i--)
            if(!horseSystem.myStacks[i].isEmpty()) {
                boardWindow.setHorse(horseSystem.myStacks[i].peek());
                System.out.println("("+ horseSystem.myStacks[i].peek().getHorizontalNumber() +","+ horseSystem.myStacks[i].peek().getVerticalNumber() +")");
                break;
            }
       for(int i=0; i<horseSystem.myStacks.length; i++)
           if(!horseSystem.myStacks[i].isEmpty()) {
               char b= (char) ((char) horseSystem.myStacks[i].peek().getVerticalNumber() +65);
               boardWindow.setStringLabels(""+ (horseSystem.myStacks[i].peek().getHorizontalNumber()+1) + b);
           }
       boardWindow.k=0;
       boardWindow.refresh();
   }

   private void getJumpsUser(){
       String sNumber = JOptionPane.showInputDialog("Ingrese la cantidad de saltos que desea: ");
       Integer number= Integer.parseInt(sNumber);
       numberJumps=number;
   }
}
