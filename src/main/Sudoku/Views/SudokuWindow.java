package main.Sudoku.Views;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class SudokuWindow extends JFrame{
    public JButton[][] myButtons= new JButton[9][9];
    public JButton solve= new JButton("Solve");
    public JButton reset= new JButton("Reset");
    private ActionListener actionListener;

    /** Creates a window for the sudoku with its configuration. An action listener is given as an argument
     *  which is used for the buttons in the window.
     */
    public SudokuWindow(ActionListener actionListener){
        this.actionListener=actionListener;
        setTitle("Sudoku Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 700);
        setLocationRelativeTo(null);
        setResizable(true);
        setVisible(true);

        JPanel thePanel = new JPanel();
        add(thePanel);
        thePanel.setLayout(new BoxLayout(thePanel, BoxLayout.X_AXIS));

        JPanel optionButtons= new JPanel();
        optionButtons.setLayout(new BoxLayout(optionButtons, BoxLayout.Y_AXIS));
        optionButtons.add(solve);
        optionButtons.add(reset);
        solve.addActionListener(actionListener);
        reset.addActionListener(actionListener);

        JPanel sudokuPanel = new JPanel();
        thePanel.add(sudokuPanel);

        sudokuPanel.setLayout(new GridLayout(9,9));
        sudokuPanel.setBackground(new Color(0,0,0));

        for(int i=0; i<9;i++) {
            for (int j = 0; j < 9; j++) {
                myButtons[i][j]= new JButton();
                myButtons[i][j].addActionListener(actionListener);
                sudokuPanel.add(myButtons[i][j]);
            }
        }
        thePanel.add(optionButtons);
    }

    /** Shows the number given to it in the window.
     */
    public void setButtonText(int number, int i, int j){
        if(number==-1)
            myButtons[i][j].setText("");
        else
            myButtons[i][j].setText(""+number);
    }
}
