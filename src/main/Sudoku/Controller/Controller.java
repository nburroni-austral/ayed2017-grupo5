package main.Sudoku.Controller;

import main.Sudoku.Model.Sudoku;
import main.Sudoku.Views.SudokuWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controller implements ActionListener{
    private SudokuWindow sudokuWindow;
    private Sudoku mySudoku;

    public Controller(){
        sudokuWindow=new SudokuWindow(this);
        mySudoku= new Sudoku();
    }

    /** Checks which button is clicked and performs the corresponding action.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Object buttonSelected = e.getSource();
        for(int i=0; i<9; i++) {
            for(int j = 0; j < 9; j++) {
                if(buttonSelected.equals(sudokuWindow.myButtons[i][j])) {
                    String snumber= JOptionPane.showInputDialog("Ingrese un numero de un solo digito (1-9): ");
                    int number= snumber.charAt(0)-48;
                    mySudoku.setNumber(number, i+1, j+1);
                    sudokuWindow.setButtonText(number, i, j);
                }
            }
        }
        if(buttonSelected.equals(sudokuWindow.solve)) {
            mySudoku.solve();
            for(int i=0; i<9; i++) {
                for (int j = 0; j < 9; j++) {
                    sudokuWindow.setButtonText(mySudoku.sudoku[i][j], i, j);
                }
            }
        }
        if(buttonSelected.equals(sudokuWindow.reset)) {
            mySudoku=new Sudoku();
            for(int i=0; i<9; i++) {
                for (int j = 0; j < 9; j++) {
                    sudokuWindow.setButtonText(-1, i, j);
                }
            }
        }
    }
}
