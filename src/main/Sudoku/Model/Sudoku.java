package main.Sudoku.Model;

import main.TP3.Stack.StaticStack;

public class Sudoku {

    public int[][] sudoku = new int[9][9];

    /** Creates a new sudoku with all values set to 0.
     */
    public Sudoku(){
        for (int i = 0; i < sudoku.length; i++){
            for (int j = 0; j < sudoku.length; j++){
                sudoku[i][j] = 0;
            }
        }
    }

    /** Solves the sudoku by calling the fillCell method.
     */
    public void solve(){
        fillCell(0, 0);
    }

    /** Allows the user to set a number in the sudoku. Before adding it to the sudoku, the method checks
     *  if the number can be added in that spot.
     */
    public void setNumber(int numberToSet, int row, int column){
            if (checkNumber(numberToSet, row - 1, column - 1)) {
                sudoku[row - 1][column - 1] = numberToSet;
            } else {
                System.out.println("No se ha podido ingresar el número ya que hay un numero igual en su columna/fila/3x3");
            }
    }


    private boolean fillCell(int row, int column){

        // If it reaches the end of a row it goes to the next one.
        if (column == sudoku.length){
            row++;
            column = 0;
        }

        // If it goes past the last spot, it returns true and finishes.
        if (row == sudoku.length){
            return true;
        }

        // If the cell is already filled with a number, it calls the method recursively for the next cell.
        if (sudoku[row][column] != 0){
            return fillCell(row, column + 1);
        }

        /* Checks every number from 1 to 9. If it is possible to fill the cell with a number, it checks if the
        method returns true for the next cell. If it does, the method returns true.
         */
        for (int numberToTry = 1; numberToTry <= 9; numberToTry++){
            if (checkNumber(numberToTry, row, column)){
                sudoku[row][column] = numberToTry;
                if (fillCell(row, column + 1)){
                    return true;
                }
            }
        }
        // If it goes through all ifs, it sets the value of the cell to 0 and returns false.
        sudoku[row][column] = 0;
        return false;

    }

    private boolean checkNumber(int numberToCheck, int row, int column){
        return checkRow(numberToCheck, row) && checkColumn(numberToCheck, column) && check3x3(numberToCheck, row, column);
    }

    private boolean checkRow(int numberToCheck, int row){
        for (int j = 0; j < sudoku.length; j++){
            if (sudoku[row][j] == numberToCheck){
                return false;
            }
        }
        return true;
    }

    private boolean checkColumn(int numberToCheck, int column){
        for (int i = 0; i < sudoku.length; i++){
            if (sudoku[i][column] == numberToCheck){
                return false;
            }
        }
        return true;
    }

    private boolean check3x3(int numberToCheck, int row, int column){
        int newRow = (row / 3) * 3;
        int newColumn = (column / 3) * 3;
        for (int i = newRow; i < newRow + 3; i++){
            for (int j = newColumn; j < newColumn + 3; j++){
                if (sudoku[i][j] == numberToCheck){
                    return false;
                }
            }
        }
        return true;
    }
}
