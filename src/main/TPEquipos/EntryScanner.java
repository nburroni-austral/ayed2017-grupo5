package main.TPEquipos;


import java.util.Scanner;

/** An EntryScanner asks for the user's input and divides the whole entry into different cases.
 */
public class EntryScanner {

    private String[][] cases;

    /** Creates a scanner and asks for and receives the entry. Once the user enters a line, it adds it to the current
     * case, creates a new case or finishes the entry, depending on what was the line entered.
     */
    public void initialize() {
        cases = new String[20][30];
        Scanner scanner = new Scanner( java.lang.System.in);
        int caseNumber = 0;
        int caseLine = 0;
        java.lang.System.out.println("Ingrese la entrada: ");
        String input = scanner.nextLine();
        while (!input.equals("-1")){
            if (input.equals("")){
                caseNumber++;
                caseLine = 0;
                input = scanner.nextLine();
                continue;
            }
            if(!input.equals("-1")) {
                cases[caseNumber][caseLine] = input;
                caseLine++;
                input = scanner.nextLine();
            }
        }
    }

    /** Returns a single case, chosen by the number given as an argument.
     */
    public String[] getCase(int caseNumber){
        return cases[caseNumber];
    }
}
