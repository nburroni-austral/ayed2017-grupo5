package main.TPEquipos;


/** A DataAnalyzer gets all the useful information from a case, organizes it and returns it in a more organized and
 * practical way.
 */
public class DataAnalyzer {

    /** Returns a case's teams in the entry's order.
     */
    public String[] getCaseTeams(String[] caseToEvaluate){
        String[] teams = new String[getNumberOfTeams(caseToEvaluate[0])];
        for (int i = 0; i < teams.length; i++){
            int spaceIndex = caseToEvaluate[i + 1].indexOf(" ");
            teams[i] = caseToEvaluate[i + 1].substring(0, spaceIndex);
        }
        return teams;
    }

    /** Returns a case's matches in the entry's order.
     */
    public String[] getCaseMatches(String[] caseToEvaluate){
        String[] matches = new String[getNumberOfMatches(caseToEvaluate[0])];
        for (int i = 0; i < matches.length; i++){
            matches[i] = caseToEvaluate[getNumberOfTeams(caseToEvaluate[0]) + 1 + i];
        }
        return matches;
    }

    /** Returns a case's points in the entry's order.
     */
    public int[] getCasePoints(String[] caseToEvaluate){
        int[] points = new int[getNumberOfTeams(caseToEvaluate[0])];
        for (int i = 0; i < points.length; i++){
            int spaceIndex = caseToEvaluate[i + 1].indexOf(" ");
            if (caseToEvaluate[i + 1].length() == spaceIndex + 3){
                points[i] = Character.getNumericValue(caseToEvaluate[i + 1].charAt(spaceIndex + 1)) * 10 + Character.getNumericValue(caseToEvaluate[i + 1].charAt(spaceIndex + 2));
            } else {
                points[i] = Character.getNumericValue(caseToEvaluate[i + 1].charAt(spaceIndex + 1));
            }
        }
        return points;
    }

    private int getNumberOfTeams(String numberOfTeamsAndMatches){
        int spaceIndex = numberOfTeamsAndMatches.indexOf(" ");
        if (spaceIndex == 2){
            return Character.getNumericValue(numberOfTeamsAndMatches.charAt(0)) * 10 + Character.getNumericValue(numberOfTeamsAndMatches.charAt(1));
        } else {
            return Character.getNumericValue(numberOfTeamsAndMatches.charAt(0));
        }
    }

    private int getNumberOfMatches(String numberOfTeamsAndMatches){
        int spaceIndex = numberOfTeamsAndMatches.indexOf(" ");
        if (numberOfTeamsAndMatches.length() == spaceIndex + 3){
            return Character.getNumericValue(numberOfTeamsAndMatches.charAt(spaceIndex + 1)) * 10 + Character.getNumericValue(numberOfTeamsAndMatches.charAt(spaceIndex + 2));
        } else {
            return Character.getNumericValue(numberOfTeamsAndMatches.charAt(spaceIndex + 1));
        }
    }


}
