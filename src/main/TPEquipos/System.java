package main.TPEquipos;

/** The System takes care of connecting the EntryScanner, DataAnalyzer and Solver so they work together.
 */
public class System {

    /** Creates an EntryScanner, DataAnalyzer and Solver and calls their needed methods. Applies the solve method to
     * every case obtained by the EntryScanner.
     */
    public void initiate(){
        EntryScanner nn = new EntryScanner();
        DataAnalyzer dd = new DataAnalyzer();
        Solver ss = new Solver();
        nn.initialize();
        int i = 0;
        java.lang.System.out.println("\nResultado: ");
        while (nn.getCase(i)[0] != null) {
            String[] caseToSolve = nn.getCase(i);
            ss.solve(dd.getCaseTeams(caseToSolve), dd.getCasePoints(caseToSolve), dd.getCaseMatches(caseToSolve));
            ss.printResults();
            i++;
        }
    }
}
