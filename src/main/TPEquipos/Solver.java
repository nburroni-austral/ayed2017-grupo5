package main.TPEquipos;


import main.TP4.Queue.DynamicQueue;

/** A Solver gets a case's data, solves the problem and prints the solution.
 */
public class Solver {


    private String[] teams;
    private int[] points;
    private int[] auxPoints;
    private String[] matches;
    private int[] results;


    /** Receives a case's data, assigns it to their respective variables and calls the solve method.
     */
    public void solve(String[] teams, int[] points, String[] matches) {
        this.teams = teams;
        this.points = points;
        this.matches = matches;
        results = new int[this.matches.length];
        for (int i = 0; i < results.length; i++){
            results[i] = 0;
        }
        auxPoints = new int[this.points.length];
        for (int i = 0; i < auxPoints.length; i++){
            auxPoints[i] = 0;
        }
        solve(0);
    }

    /** Solves the problem by backtracking.
     */
    public boolean solve(int matchNumber){
        DynamicQueue<Integer> possibleResults = new DynamicQueue<>();

        if (matchNumber == matches.length){
            for (int i = 0; i < points.length; i++){
                if (points[i] != auxPoints[i]){
                    return false;
                }
            }
            return true;
        }
        String homeTeam = getHomeTeam(matches[matchNumber]);
        String awayTeam = getAwayTeam(matches[matchNumber]);

        if (auxPoints[getTeamPosition(homeTeam)] <= points[getTeamPosition(homeTeam)] - 3){
            possibleResults.enqueue(1);
        }
        if (auxPoints[getTeamPosition(awayTeam)] <= points[getTeamPosition(awayTeam)] - 3){
            possibleResults.enqueue(2);
        }
        if (auxPoints[getTeamPosition(homeTeam)] <= points[getTeamPosition(homeTeam)] - 1 && auxPoints[getTeamPosition(awayTeam)] <= points[getTeamPosition(awayTeam)] - 1){
            possibleResults.enqueue(3);
        }

        while (!possibleResults.isEmpty()){
            results[matchNumber] = possibleResults.dequeue();
            if (results[matchNumber] == 1){
                auxPoints[getTeamPosition(homeTeam)] += 3;
            } else if (results[matchNumber] == 2){
                auxPoints[getTeamPosition(awayTeam)] += 3;
            } else {
                auxPoints[getTeamPosition(homeTeam)] += 1;
                auxPoints[getTeamPosition(awayTeam)] += 1;
            }
            if (solve(matchNumber + 1)){
                return true;
            } else {
                if (results[matchNumber] == 1){
                    auxPoints[getTeamPosition(homeTeam)] -= 3;
                } else if (results[matchNumber] == 2){
                    auxPoints[getTeamPosition(awayTeam)] -= 3;
                } else {
                    auxPoints[getTeamPosition(homeTeam)] -= 1;
                    auxPoints[getTeamPosition(awayTeam)] -= 1;
                }
            }
        }
        results[matchNumber] = 0;
        return false;
    }

    /** Prints the matches' results.
     */
    public void printResults(){
        for (int i = 0; i < results.length; i++){
            if (results[i] == 3){
                java.lang.System.out.print("X ");
                continue;
            }
            java.lang.System.out.print(results[i] + " ");
        }
        java.lang.System.out.println("");
    }


    private int getTeamPosition(String team){
        for (int i = 0; i < teams.length; i++){
            if (teams[i].equals(team)){
                return i;
            }
        }
        return -1;
    }

    private String getHomeTeam(String match){
        int spaceIndex = match.indexOf(" ");
        return match.substring(0, spaceIndex);
    }

    private String getAwayTeam(String match){
        int spaceIndex = match.indexOf(" ");
        return match.substring(spaceIndex + 1, match.length());
    }

}
