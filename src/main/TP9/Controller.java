package main.TP9;

import main.ListImplementations.DynamicList;

import java.io.*;

/**
 * Created by JoseRojas on 10/12/17
 */

public class Controller {
    Dictionary dictionary;
    BufferedReader fileReader;
    DynamicList<Error> words;
    Console console;

    String direction;

    public Controller() {
        console = new Console();
        dictionary = new Dictionary("src/main/TP9/resources/20kWords.txt");
        direction = console.initialize();
        find(direction);
        controller(console.menu());
    }

    private void controller(int number) {
        switch (number) {
            case 0:
                break;
            case 1:
                addWord(console.addWord());
                controller(console.menu());
                break;
            case 2:
                find(direction);
                controller(console.menu());
                break;
            default:
                console.error();
                controller(console.menu());
                break;
        }
    }

    private void find(String str) {
        words = new DynamicList<>();
        try {
            FileReader file = new FileReader(str);
            fileReader = new BufferedReader(file);
        } catch (IOException e) {
            System.out.println("The file can't be read");
        }
        search();
        write();
    }

    private void addWord(String word) {
        dictionary.addWord(word);
    }

    private void search() {
        try {
            String line;
            int indexLine = 1;
            while ((line = fileReader.readLine()) != null) {
                searchWork(line, indexLine);
                indexLine++;
            }
        } catch (IOException e) {
            System.out.println("The file can't be read");
        }
    }

    private void searchWork(String phrase, int index) {
        String word = "";
        for (int i = 0; i < phrase.length(); i++) {
            char aux = phrase.charAt(i);
            switch (aux) {
                case ' ':
                    if (!dictionary.containsWord(word.toLowerCase())) {
                        Error error = new Error();
                        error.word = word;
                        error.indexLine = index;
                        words.insertNext(error);
                    }
                    word = "";
                    break;

                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '!':
                case '?':
                case ';':
                case ',':
                case '.':
                    break;

                default:
                    word += aux;
                    break;
            }
        }
    }

    private void write() {
        try {
            FileWriter fileWriter = new FileWriter("src/main/TP9/resources/ErrorWords.txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);

            for (int i = 0; i < words.size(); i++) {
                words.goTo(i);
                String theWORD = words.getActual().word;
                String phrase = "\nLine " + words.getActual().indexLine + " -> " + theWORD + ": ";
                printWriter.println(phrase);
                DynamicList<String> similarWords = dictionary.similarWords(theWORD);
                if (similarWords != null) {
                    for (int j = 0; j < similarWords.size(); j++) {
                        similarWords.goTo(j);
                        printWriter.println("                       *" + similarWords.getActual());
                    }
                } else {
                    printWriter.println("                       *No matches");
                }
            }

            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static class Error {
        String word;
        int indexLine;
    }
}
