package main.TP9;

import main.ListImplementations.DynamicList;
import struct.istruct.list.List;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by JoseRojas on 10/12/17
 */

public class Dictionary {
    private DynamicList<String>[] hashTable;

    public Dictionary(String dictionary) {
        hashTable = new DynamicList[26000];
        try {
            FileReader file = new FileReader(dictionary);
            BufferedReader buffer = new BufferedReader(file);
            String line;
            while ((line = buffer.readLine()) != null)
                addWord(line);
            buffer.close();
        } catch (IOException e) {
            System.out.println("The file can't be read");
        }
    }

    public void addWord(String word) {
        int index = hashing(word);
        if (hashTable[index] == null)
            hashTable[index] = new DynamicList<>();
        if (!containsWord(word))
            hashTable[index].insertNext(word);
    }

    public DynamicList<String> similarWords(String word) {
        return hashTable[hashing(word)];
    }

    private int hashing(String word) {
        String str = Soundex.soundex(word);
        return Integer.parseInt(((int) str.charAt(0) - 'A') + str.substring(1, 4));
    }

    boolean containsWord(String word) {
        DynamicList<String> list = hashTable[hashing(word)];
        if (list == null)
            return false;
        for (int i = 0; i < list.size(); i++) {
            list.goTo(i);
            if (list.getActual().equals(word))
                return true;
        }
        return false;
    }
}
