package main.TP9;

import java.util.Scanner;

/**
 * Created by JoseRojas on 11/12/17
 */

class Console {
    private Scanner scanner;

    Console() {
        scanner = new Scanner(System.in);
    }

    String initialize() {
        System.out.println("\n\n                    CORRECTOR ORTOGRÁFICO");
        scanner = new Scanner(System.in);
        System.out.println("\n \n \nIngrese la dirección del texto a corregir: ");
        String dir = scanner.nextLine();
        System.out.println("\nEn la dirección 'src/main/TP9/resources/ErrorWords.txt' encontrara los errores a corregir.");
        return dir;
    }

    int menu() {
        System.out.println("\nPresione:  \n0 Para salir. \n1 Para agregar una palabra al diccionario. \n2 Para corregir de nuevo.");
        return scanner.nextInt();
    }

    String addWord() {
        scanner = new Scanner(System.in);
        System.out.println("\nIngrese la palabra a agregar: ");
        return scanner.nextLine().toLowerCase();
    }

    void error() {
        System.out.println("\nError!!");
    }
}
