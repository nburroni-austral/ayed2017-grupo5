package main.TP5_Shilton;


import main.TP4.Queue.DynamicQueue;

public class Window {

    private int idleTime;
    private int clientWaitedTime;
    private int numberOfClientsAttended;
    private Double price = 0.70;
    private Double earnedMoney;
    private DynamicQueue<Client> clientsQueue;

    /** Creates a window with its counters in 0.
     */
    public Window() {
        idleTime = 0;
        clientWaitedTime = 0;
        numberOfClientsAttended = 0;
        earnedMoney = 0.0;
        clientsQueue = new DynamicQueue<>();
    }

    /** Adds a client to the queue.
     */
    public void addClient(Client clientToAdd){
        clientsQueue.enqueue(clientToAdd);
    }

    /** Checks if there are clients to attend. If there aren't, it adds to the idle time. If there is, rolls to check
     * if the client will be attended or not.
     */
    public Client attendClient(){
        if (clientsQueue.isEmpty()){
            idleTime += 1;
            return null;
        } else {
            if (Math.random() < 0.30) {
                earnedMoney += price;
                numberOfClientsAttended += 1;
                return clientsQueue.dequeue();
            } else {
                return null;
            }
        }
    }

    /** Attends clients in the last 30 seconds.
     */
    public Client attendLastClients() {
        earnedMoney += price;
        numberOfClientsAttended += 1;
        return clientsQueue.dequeue();
    }

    /** Adds how much time last attended client waited to the counter.
     */
    public void addClientWaitedTime(int cyclesWaited){
        clientWaitedTime += cyclesWaited;
    }

    /** Checks if there are clients left in the queue.
     */
    public boolean hasClientsInQueue(){
        return !clientsQueue.isEmpty();
    }

    /** Returns money earned.
     */
    public Double getEarnedMoney() {
        return earnedMoney;
    }

    /** Calculates an average of time waited by client.
     */
    public int getAverageWaitedTimeByClient(){
        return (clientWaitedTime * 10) / numberOfClientsAttended;
    }

    /** Returns the window's idle time.
     */
    public int getIdleTime() {
        return idleTime * 10;
    }
}
