package main.TP5_Shilton;


public class Client {

    private int arrivalTime;

    /** Creates a client which knows his arrival time.
     */
    public Client(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /** Return the client's arrival time.
     */
    public int getArrivalTime() {
        return arrivalTime;
    }
}
