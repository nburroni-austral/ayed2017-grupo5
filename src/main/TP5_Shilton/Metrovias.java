package main.TP5_Shilton;


import main.ListTypes.List.DynamicList;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Metrovias {

    private int startingHour = 6;
    private int closingHour = 22;
    private DynamicList<Window> windows;
    private int numberOfCycles;
    private int currentCycle;


    /** Initializes the system. Asks for the system's number of windows and starts the simulation.
     */
    public void initialize(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el número de ventanas deseado: ");
        int numberOfWindows = scanner.nextInt();
        while (numberOfWindows < 3 || numberOfWindows > 10) {
            System.out.println("Ingrese un número de ventanillas entre 3 y 10");
            numberOfWindows = scanner.nextInt();
        }
        scanner.close();
        numberOfCycles = ((closingHour - startingHour) * 3600) / 10;
        windows = new DynamicList<Window>();
        for (int i = 0; i < numberOfWindows; i++){
            windows.insertNext(new Window());
        }
        simulate();
    }

    //Simulates every cycle. Then simulates the last 30 seconds and prints the result data.
    private void simulate(){
        for (int i = 1; i < numberOfCycles - 3; i++){
            currentCycle = i;
            addClients();
            attendClients();
        }
        finishDay();
        printData();
    }

    //Creates the clients and adds them to the queues randomly.
    private void addClients(){
        for (int i = 0; i < 5; i++){
            int random = (int) (Math.random() * 10);
            while (random > windows.size() - 1){
                random = (int) (Math.random() * 10);
            }
            windows.goTo(random);
            windows.getActual().addClient(new Client(currentCycle));
        }
    }

    //Attends clients in the normal cycles.
    private void attendClients(){
        for (int j = 0; j < windows.size(); j++){
            windows.goTo(j);
            Client attendedClient = windows.getActual().attendClient();
            if (attendedClient != null){
                windows.getActual().addClientWaitedTime(currentCycle - attendedClient.getArrivalTime());
            }
        }
    }

    //Attends the remaining clients in the last 30 seconds.
    private void finishDay(){
        for (int i = 0; i < windows.size(); i++){
            windows.goTo(i);
            while (windows.getActual().hasClientsInQueue()){
                Client attendedClient = windows.getActual().attendLastClients();
                windows.getActual().addClientWaitedTime(numberOfCycles - attendedClient.getArrivalTime());
            }
        }
    }

    //Prints the obtained data.
    private void printData(){
        for (int i = 0; i < windows.size(); i++){
            windows.goTo(i);
            System.out.println("Ventanilla " + (i + 1) + ": \n");
            System.out.println("Tiempo media de espera de los clientes: " + (windows.getActual().getAverageWaitedTimeByClient() / 60) + " minutos");
            System.out.println("Monto total recaudado: " + new DecimalFormat("##.00").format(windows.getActual().getEarnedMoney()));
            System.out.println("Tiempo ocioso: " + windows.getActual().getIdleTime() + " segundos");
            System.out.println("\n --------------------------------------------------------------- \n");
        }
    }
}
