package main.TP3.LexicographicAnalyzer;


import main.TP3.Stack.StaticStack;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/** Creates a lexicographic analyzer with a single method.
 */
public class LexicographicAnalyzer {

    /** Reads the text file and looks for {, [ and (. When it finds one of those, it stores it in a stack.
     *  When it finds a }, ] or ), it checks if the top symbol in the stack is its counterpart. If this is the case,
     *  the method keeps reading the file. If not, it stops and returns false. If it reaches the end of the file, it
     *  checks if the stack is empty. If it is it returns true, if not it returns false.
     */
    public boolean AnalyzeText(File textFile) throws IOException{
        try (BufferedReader br = new BufferedReader(new FileReader(textFile))) {
            String line;
            StaticStack<Character> openedSymbols = new StaticStack<>();
            while ((line = br.readLine()) != null) {
                for (int i = 0; i < line.length(); i++){
                    if (line.charAt(i) == '{' || line.charAt(i) == '[' || line.charAt(i) == '('){
                        openedSymbols.push(line.charAt(i));
                    } else if (line.charAt(i) == ')'){
                        if (openedSymbols.isEmpty()){
                            return false;
                        }
                        if (openedSymbols.peek() == '('){
                            openedSymbols.pop();
                        } else {
                            return false;
                        }
                    } else if (line.charAt(i) == ']') {
                        if (openedSymbols.isEmpty()){
                            return false;
                        }
                        if (openedSymbols.peek() == '[') {
                            openedSymbols.pop();
                        } else {
                            return false;
                        }
                    }  else if (line.charAt(i) == '}'){
                        if (openedSymbols.isEmpty()){
                            return false;
                        }
                        if (openedSymbols.peek() == '{'){
                            openedSymbols.pop();
                        } else {
                            return false;
                        }
                    }
                }
            }
            if (openedSymbols.isEmpty()){
                return true;
            }
            return false;
        }
    }


}
