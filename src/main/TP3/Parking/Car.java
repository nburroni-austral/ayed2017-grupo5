package main.TP3.Parking;

/** Creates a car with its attributes.
 */
public class Car {
    private String licensePlate;
    private String color;
    private String brand;
    private String model;

    public Car(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }
}
