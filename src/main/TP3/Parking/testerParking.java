package main.TP3.Parking;


public class testerParking {
    public static void main(String[] args){
        Parking myParking = new Parking();
        Car car1 = new Car("mcp 476");
        Car car2 = new Car("mcp 477");
        Car car3 = new Car("mcp 478");
        Car car4 = new Car("mcp 479");
        Car car5 = new Car("mcp 480");
        Car car6 = new Car("mcp 481");
        Car car7 = new Car("mcp 482");

        System.out.println("Is empty: "+myParking.isEmpty());

        myParking.add(car1);
        myParking.add(car2);
        myParking.add(car3);
        myParking.add(car4);
        myParking.add(car5);
        myParking.add(car6);
        myParking.add(car7);

        System.out.println("Is empty: "+myParking.isEmpty());
        myParking.printCarParking();
        System.out.println("\n");

        myParking.remove("mcp 476");
        myParking.remove("mcp 478");
        myParking.remove("mcp 481");

        myParking.printCarParking();
        myParking.empty();
        System.out.println("\n");
        System.out.println("Is empty: "+myParking.isEmpty());

        System.out.println("La ganancias son: §"+myParking.getTodaysProfit());
        myParking.printTodaysList();
    }
}
