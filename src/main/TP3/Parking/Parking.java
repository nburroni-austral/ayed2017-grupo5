package main.TP3.Parking;

import main.TP3.Stack.DynamicStack;

import java.util.ArrayList;

/** Creates a parking lot.
 */
public class Parking {
    private DynamicStack<Car> parking;
    private DynamicStack<Car> street;
    private ArrayList<String> carListToday= new ArrayList<>();
    private int quantity;

    public Parking(){
        parking= new DynamicStack<>();
        street= new DynamicStack<>();
    }

    /** Adds a car to the parking lot by adding it to the parking stack.
     */
    public void add(Car c){
        if(quantity < 50) {
            parking.push(c);
            carListToday.add(c.getLicensePlate());
            quantity++;
        }
        else
            System.out.println("No Place");
    }

    /** Removes a car from the parking lot by removing it from the parking stack.
     */
    public Car remove(String licensePlate){
        while(!(parking.peek().getLicensePlate().equals(licensePlate))) {
            street.push(parking.peek());
            parking.pop();
        }
        Car c = parking.peek();
        parking.pop();
        while (!(street.isEmpty())){
            parking.push(street.peek());
            street.pop();
        }
        return c;
    }

    /** Calculates today's earnings.
     */
    public int getTodaysProfit(){
        return carListToday.size()*5;
    }

    /** Prints a list of every car parked today.
     */
    public void printTodaysList(){
        for (String i: carListToday)
            System.out.println("Patente del auto: "+i);
    }

    /** Checks if the parking lot is empty.
     *  Returns true if the parking lot is empty and false otherwise.
     */
    public boolean isEmpty(){
        return parking.isEmpty();
    }


    /** Empties the parking lot by emptying the parking stack.
     */
    public void empty(){
        parking.empty();
    }

    /** Prints a list of the cars which are parked at the moment.
     */
    public void printCarParking(){
        while (!(parking.isEmpty())){
            System.out.println("La patente del auto: "+parking.peek().getLicensePlate());
            street.push(parking.peek());
            parking.pop();
        }
        while (!(street.isEmpty())){
            parking.push(street.peek());
            street.pop();
        }
    }
}
