package main.TP3.Stack;

import main.TP3.Stack.DynamicStack;
import main.TP3.Stack.StaticStack;


public class StackTester {
    public static void main(String[] args){
        System.out.println("Con pila estatica: ");
        StaticStack<Integer> test= new StaticStack();
        System.out.println(test.peek());
        System.out.println(test.isEmpty());

        test.push(3);
        test.push(4);
        test.push(5);
        System.out.println(test.peek());
        test.pop();
        System.out.println(test.peek());
        System.out.println(test.isEmpty());
        System.out.println(test.size());

        test.empty();
        System.out.println(test.isEmpty());

        test.push(3);
        test.push(4);
        test.push(5);
        test.push(3);
        test.push(4);
        test.push(5);
        test.push(3);
        test.push(4);
        System.out.println(test.peek());
        System.out.println(test.size() + "\n");

        System.out.println("Con pila dinamica: ");
        DynamicStack<Integer> test2= new DynamicStack<>();
        System.out.println(test2.peek());
        System.out.println(test2.isEmpty());

        test2.push(3);
        test2.push(4);
        test2.push(5);
        System.out.println(test2.peek());
        test2.pop();
        System.out.println(test2.peek());
        System.out.println(test2.isEmpty());
        System.out.println(test2.size());

        test2.empty();
        System.out.println(test2.isEmpty());

        test2.push(3);
        test2.push(4);
        test2.push(5);
        test2.push(3);
        test2.push(4);
        test2.push(5);
        test2.push(3);
        test2.push(4);
        System.out.println(test2.peek());
        System.out.println(test2.size());

        test2.empty();
        System.out.println(test2.isEmpty());
    }
}
