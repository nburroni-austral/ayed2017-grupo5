package main.TP3.Stack;


public class Nodo<T> {
    private T obj;
    public Nodo next;

    public T getObj() {
        return obj;
    }
    public void setObj(T obj) {
        this.obj = obj;
    }
}