package main.TP3.Stack;

import struct.istruct.Stack;


public class DynamicStack<T> implements Stack<T>{
    private int size=0;
    private Nodo<T> top;

    public DynamicStack(){
        top = new Nodo<T>();
    }



    /** Agrega un objeto a la pila.
     * */
    @Override
    public void push(T o) {
        top.setObj(o);
        Nodo<T> aux = new Nodo<T>();
        aux.next = top;
        top =aux;
        size++;
    }

    /** Retira el ultimo objeto de la pila.
     * */
    @Override
    public void pop() {
        size--;
        if(size == -1) {
            size = 0;
            top.next=top;
        }
        top = top.next;
    }


    /** Te muestra el objeto que esta en la ultima posicion.
     * */
    @Override
    public T peek() {
        if(size==0)
            return null;
        return (T) (top.next).getObj();
    }


    /** Devuelve true si la pila esta vacia.
     * */
    @Override
    public boolean isEmpty() {
        return ( (size==0) ? true : false );
    }


    /** Devuelve el tamaño de la pila.
     * */
    @Override
    public int size() {
        return size;
    }


    /** Vacia la pila.
     * */
    @Override
    public void empty() {
        top=new Nodo<T>();
        size=0;
    }
}
