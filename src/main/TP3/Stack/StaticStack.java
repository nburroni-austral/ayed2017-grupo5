package main.TP3.Stack;

import struct.istruct.Stack;


public class StaticStack<T> implements Stack<T>{
    private int size=0;
    private Object[] stack;

    public StaticStack(){
        stack = new Object[50];
    }


    /** Agrega un objeto a la pila.
     * */
    @Override
    public void push(T o) {
        if(size == stack.length)
            grow();
        size++;
        stack[size-1]= o;
    }


    /** Retira el ultimo objeto de la pila.
     * */
    @Override
    public void pop() {
        size--;
        if(size == -1)
            size=0;
    }

    /** Te muestra el objeto que esta en la ultima posicion.
     * */
    @Override
    public T peek() {
        if(size==0)
            return null;
        return (T) stack[size-1];
    }

    /** Devuelve true si la pila esta vacia.
     * */
    @Override
    public boolean isEmpty() {
        return size==0;
    }

    /** Devuelve el tamaño de la pila.
     * */
    @Override
    public int size() {
        return size;
    }

    /** Vacia la pila.
     * */
    @Override
    public void empty() {
        size=0;
    }

    /** Si la pila se queda sin memoria, le duplica la memoria.
     * */
    private void grow(){
        Object[] stack2 = new Object[2*stack.length];
        for (int i =0; i<stack.length;i++){
            stack2[i] = stack[i];
        }
        stack = stack2;
    }
}
