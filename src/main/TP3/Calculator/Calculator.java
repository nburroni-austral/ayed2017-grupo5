package main.TP3.Calculator;

import main.TP3.Stack.StaticStack;

import java.util.Scanner;

/** Creates a calculator which takes simple instructions through a String.
 */
public class Calculator {

    private String instructions;

    public Calculator(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter operation: ");
        instructions=scanner.nextLine();
    }

    /** Asks for the instructions from input and then does all the specified math operations.
     */
    public Double calculate(){
        analyze();
        StaticStack<Object> instructionsStack = ReOrder.inicialitation(instructions);
        instructionsStack = doMultiplications(instructionsStack);
        instructionsStack = doDivisions(instructionsStack);
        instructionsStack = doAdditionsAndSubtractions(instructionsStack);

        return (Double) instructionsStack.peek();
    }

    private StaticStack<Object> doMultiplications(StaticStack<Object> instructions){
        StaticStack<Object> auxStack = createAux(instructions);
        instructions.empty();
        instructions.push(auxStack.peek());
        auxStack.pop();
        while (!auxStack.isEmpty()){
            if (auxStack.peek().equals('*')){
                auxStack.pop();
                Double number2 = (Double) auxStack.peek();
                Double number1 = (Double) instructions.peek();
                auxStack.pop();
                instructions.pop();
                instructions.push(number1 * number2);
            } else {
                instructions.push(auxStack.peek());
                auxStack.pop();
            }
        }
        return instructions;
    }

    private StaticStack<Object> doDivisions(StaticStack<Object> instructions) {
        StaticStack<Object> auxStack = createAux(instructions);
        instructions.push(auxStack.peek());
        auxStack.pop();
        while (!auxStack.isEmpty()) {
            if (auxStack.peek().equals('/')) {
                auxStack.pop();
                Double number2 = (Double) auxStack.peek();
                Double number1 = (Double) instructions.peek();
                auxStack.pop();
                instructions.pop();
                instructions.push(number1 / number2);
            } else {
                instructions.push(auxStack.peek());
                auxStack.pop();
            }
        }
        return instructions;
    }

    private StaticStack<Object> doAdditionsAndSubtractions(StaticStack<Object> instructions) {
        StaticStack<Object> auxStack = createAux(instructions);
        instructions.push(auxStack.peek());
        auxStack.pop();
        while (!auxStack.isEmpty()) {
            if (auxStack.peek().equals('+')) {
                auxStack.pop();
                Double number2 = (Double) auxStack.peek();
                Double number1 = (Double) instructions.peek();
                auxStack.pop();
                instructions.pop();
                instructions.push(number1 + number2);
            } else {
                auxStack.pop();
                Double number2 = (Double) auxStack.peek();
                Double number1 = (Double) instructions.peek();
                auxStack.pop();
                instructions.pop();
                instructions.push(number1 - number2);
            }
        }
        return instructions;
    }

    private StaticStack<Object> createAux(StaticStack<Object> stackToCreateAuxFrom){
        StaticStack<Object> aux = new StaticStack<>();
        while (!stackToCreateAuxFrom.isEmpty()){
            aux.push(stackToCreateAuxFrom.peek());
            stackToCreateAuxFrom.pop();
        }
        return aux;
    }


    //Checks if the instructions are well written, if not it asks for a new instruction.
    private void analyze(){
        for(int i=0; i<instructions.length(); i++)
            if((instructions.charAt(i) < 48) && (instructions.charAt(i) > 41) && (instructions.charAt(i+1) < 48) && (instructions.charAt(i+1) > 41)) {
                Scanner sc = new Scanner(System.in);
                System.out.println("Syntax error. Try again: ");
                instructions=sc.nextLine();
                calculate();
            }
    }
}