package main.TP3.Calculator;

import main.TP3.Stack.StaticStack;

/** This class is filled with methods which take care of receiving the instructions as a String and then
 *  returning them as a stack of Doubles and Characters.
 */


public class ReOrder {
    private static StaticStack<Object> stackOp;
    private static StaticStack<Double> aux;


    /** This method separates the elements from the String into a Double stack and a Character one.
     *  After this, it calls the order method.
     */
    public static StaticStack<Object> inicialitation(String op){
        stackOp=new StaticStack<>();
        aux=new StaticStack<>();
        for(int i =0; i<op.length(); i++){
            if( (op.charAt(i) < 58) && (op.charAt(i) > 47) ){
                Double number = op.charAt(i)-48d;
                stackOp.push(number);
            }
            if( (op.charAt(i) < 48) && (op.charAt(i) > 41) ){
                Character character = op.charAt(i);
                stackOp.push(character);
            }
        }
        order();
        return stackOp;
    }


    private static void order(){
        StaticStack<Object> auxStack=new StaticStack<>();
        int j=0;
        while( !(stackOp.isEmpty()) ){
            if(isNumber())
                aux.push(newNumber((double) stackOp.peek(), j));
            if( (!isNumber())) {
                auxStack.push(auxSum());
                auxStack.push(stackOp.peek());
                aux.empty();
                j=-1;
            }
            if(stackOp.size()==1)
                auxStack.push(auxSum());
            stackOp.pop();
            j++;
        }
        while(!(auxStack.isEmpty())){
            stackOp.push(auxStack.peek());
            auxStack.pop();
        }
    }



    //This method checks if the top Object in the stack is a number.
    private static boolean isNumber(){
        return  ( stackOp.peek().getClass().equals(Double.class) );
    }



    /* This method gets a Double and multiplies it by 10 powered by i. It is used to get the Doubles
     with more than one digit.
      */
    private static Double newNumber(Double num ,int i){
        return (num * Math.pow(10,i));
    }


    /** Este metodo devuelve la suma de todos los Doubles que tiene la pila aux
     * */

    private static Double auxSum(){
        Double n = 0d;
        while (!(aux.isEmpty())){
            n = n + aux.peek();
            aux.pop();
        }
        return n;
    }
}
