package main.TP11;

/**
 * Created by Franco Palumbo on 25-Jun-17
 */

public class Song {
    private char band;
    private int secondsLong;
    private int yearReleased;
    private boolean isActive;

    public Song(char band, int secondsLong, int yearReleased, boolean isActive) {
        this.band = band;
        this.secondsLong = secondsLong;
        this.yearReleased = yearReleased;
        this.isActive = isActive;
    }

    public char getBand() {
        return band;
    }

    public int getSecondsLong() {
        return secondsLong;
    }

    public int getYearReleased() {
        return yearReleased;
    }

    public boolean isActive() {
        return isActive;
    }

    @Override
    public String toString() {
        return "Song{" +
                "band=" + band +
                ", secondsLong=" + secondsLong +
                ", yearReleased=" + yearReleased +
                '}';
    }


    public void setBand(char band) {
        this.band = band;
    }

    public void setSecondsLong(int secondsLong) {
        this.secondsLong = secondsLong;
    }

    public void setYearReleased(int yearReleased) {
        this.yearReleased = yearReleased;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
