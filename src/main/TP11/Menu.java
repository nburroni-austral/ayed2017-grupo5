package main.TP11;

import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Franco Palumbo on 25-Jun-17
 */

public class Menu {
    public static void main(String[] args){
        try {
            DataFile dataFile = new DataFile("My playList");
            dataFile.write(new Song('A',233,2010,true));
            dataFile.write(new Song('B',261,2007,true));
            dataFile.write(new Song('A',200,2015,true));

            dataFile.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }

        Scanner myScanner = new Scanner(System.in);
        System.out.println(" Add a song = 1 \n Delete a song = 2 \n Modify a song = 3 \n Show specific information = 4 \n Show report = 5 \n Exit = 0 ");
        int selectedNumber = myScanner.nextInt();
        while (selectedNumber!=0){
            if(selectedNumber==1){
                System.out.println("Song band: ");
                char band = (myScanner.next()).charAt(0);
                System.out.println("Song length in seconds: ");
                int length = myScanner.nextInt();
                System.out.println("Song year released: ");
                int yearReleased = myScanner.nextInt();

                Song song = new Song(band,length,yearReleased,true);
                try{
                    addSong(song);

                }catch (IOException e){
                    e.getMessage();
                }

            }
            if(selectedNumber==2){
                System.out.println("Song band: ");
                char band = (myScanner.next()).charAt(0);
                System.out.println("Song length in seconds: ");
                int length = myScanner.nextInt();
                try {
                    deleteSong(band,length);
                }catch (IOException e){
                    e.getMessage();
                }
            }
            if(selectedNumber==3){
                System.out.println("Band song to be modify: ");
                char band = (myScanner.next()).charAt(0);
                System.out.println("Song length in seconds to be modify: ");
                int length = myScanner.nextInt();

                System.out.println("New song band name: ");
                char newBand = myScanner.next().charAt(0);
                System.out.println("New song length in seconds: ");
                int newLength = myScanner.nextInt();
                System.out.println("New song year released: ");
                int newYearReleased = myScanner.nextInt();

                Song newSong = new Song(newBand,newLength,newYearReleased,true);

                try{
                    modifySong(band,length,newSong);

                }catch (IOException e){
                    e.getMessage();
                }
            }
            if(selectedNumber==4){
                System.out.println("Show specific song = 1 \n Show amount of songs = 2 \n Show amount of songs of specific band = 3 \n Go back = 4 \n Exit = 0");
                int selectedNumber2 = myScanner.nextInt();
                if(selectedNumber2==1){
                    System.out.println("Band song to be modify: ");
                    char band = (myScanner.next()).charAt(0);
                    System.out.println("Song length in seconds to be modify: ");
                    int length = myScanner.nextInt();

                    try{
                        Song aSong = searchSong(band,length);
                        System.out.println(aSong);
                    }catch (IOException e){
                        e.getMessage();
                    }
                }
                if(selectedNumber2==2){
                    try {
                        System.out.println("Amount of songs: "+amountOfSongs());
                    }catch (IOException e){
                        e.getMessage();
                    }
                }
                if(selectedNumber2==3){
                    System.out.println("Song band: ");
                    char band = (myScanner.next()).charAt(0);
                    try {
                        System.out.println("Amount of songs of band "+band+": "+songsOfBand(band));
                    }catch (IOException e){
                        e.getMessage();
                    }
                }
                if (selectedNumber2==4){
                    continue;
                }
                if (selectedNumber2==0){
                    break;
                }
            }
            if (selectedNumber==5){
                System.out.println("Show all songs = 1 \n Show all songs of a specific band = 2 \n Go back = 3 \n Exit = 0");
                int selectedNumber3 = myScanner.nextInt();
                if (selectedNumber3 == 1){
                    try {
                        showAllSongs();
                    }catch (IOException e){
                        e.getMessage();
                    }

                }
                if(selectedNumber3 == 2){
                    System.out.println("Song band: ");
                    char band = (myScanner.next()).charAt(0);
                    try{
                        showSpecificSongs(band);
                    }catch (IOException e){
                        e.getMessage();
                    }
                }
                if(selectedNumber3 == 3){
                    continue;
                }
                if (selectedNumber3 == 4){
                    break;
                }
            }
            if(selectedNumber==0){
                break;
            }
            System.out.println(" Add a song = 1 \n Delete a song = 2 \n Modify a song = 3 \n Show specific information = 4 \n Show report = 5 \n Exit = 0 ");
            selectedNumber = myScanner.nextInt();
        }





    }
    public static long amountOfSongs() throws IOException{
        DataFile dataFile = new DataFile("My playList");
        long l = dataFile.amountOfRegistries();
        return l;
    }
    public static void addSong(Song song)throws IOException{
        DataFile dataFile = new DataFile("My playList");
        dataFile.endOfFile();
        dataFile.write(song);
        dataFile.close();
    }
    public static Song searchSong(char band, int secondsLong) throws IOException{
        DataFile dataFile = new DataFile("My playList");
        Song song = dataFile.searchByBandAndLength(band,secondsLong);
        return song;
    }
    public static int songsOfBand(char band) throws IOException{
        DataFile dataFile = new DataFile("My playList");
        return dataFile.amountOfSongsByBand(band);
    }
    public static boolean deleteSong(char band, int secondsLong) throws IOException{
        DataFile dataFile = new DataFile("My playList");
        return dataFile.deleteSong(band,secondsLong);
    }
    public static boolean modifySong(char band, int secondsLong, Song s) throws IOException{
        DataFile dataFile = new DataFile("My playList");
        return dataFile.modifySong(band,secondsLong,s);
    }
    public static void showAllSongs()throws IOException{
        DataFile dataFile = new DataFile("My playList");
        dataFile.listSongs();
    }
    public static void showSpecificSongs(char band) throws IOException {
        DataFile dataFile = new DataFile("My playList");
        dataFile.listSpecificSongs(band);
    }

}
