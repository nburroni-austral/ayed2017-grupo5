package main.TP11;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by Franco Palumbo on 25-Jun-17
 */

public class DataFile {
    private File f;
    private RandomAccessFile raf;
    private int registrySize = 11;

    public DataFile(String name) throws FileNotFoundException{
        f = new File(name);
        raf = new RandomAccessFile(f,"rw");
    }

    public void write(Song song) throws IOException{
        raf.writeChar(song.getBand());
        raf.writeInt(song.getSecondsLong());
        raf.writeInt(song.getYearReleased());
        raf.writeBoolean(song.isActive());
    }
    public void close() throws IOException{
        raf.close();
    }
    public long length() throws IOException{
        return raf.length();
    }
    public Song read()  throws IOException{
        return new Song(raf.readChar(),raf.readInt(),raf.readInt(),raf.readBoolean());
    }
    public long amountOfRegistries() throws IOException{
        return raf.length()/registrySize;
    }
    public void listSongs(){
        Song s;
        try{

            long amount = amountOfRegistries();
            startOfFile();
            for(long i=0;i<amount;i++) {
                s = read();
                if (s.isActive()) {
                    System.out.println(s);
                }

            }
            close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
    public void listSpecificSongs(char band){
        Song s;
        try {

            long amount = amountOfRegistries();
            startOfFile();
            for(long l=0;l<amount;l++){
                s = read();
                if(s.isActive() && s.getBand()==band){
                    System.out.println(s);
                }
            }
        }catch (IOException e){
            e.getMessage();
        }
    }

    public void startOfFile()throws IOException{
        raf.seek(0);
    }
    public void endOfFile() throws IOException{
        raf.seek(raf.length());
    }
    public void goTo(long place) throws IOException{
        raf.seek((place-1)*registrySize);
    }
    public Song searchByBandAndLength(char band,int length) throws IOException{

        long amount = amountOfRegistries();
        startOfFile();
        Song s;
        startOfFile();
        for (long l=0;l<amount;l++){
            s = read();
            if(s.getBand()==band && s.getSecondsLong()==length && s.isActive()==true){
                System.out.println("Song was found");
                return s;
            }
        }
        System.out.println("Song was not found");
        return null;
    }
    public int amountOfSongsByBand(char band) throws IOException{
        long amount = amountOfRegistries();
        startOfFile();
        int aux = 0;
        Song s;

        for (long l = 0;l<amount;l++){
            s = read();
            if(s.getBand()==band && s.isActive()==true){
                aux++;
            }
        }
        return aux;
    }
    public boolean deleteSong(char band, int secondsLong)throws IOException{
        startOfFile();
        Song s = searchByBandAndLength(band,secondsLong);
        if(s!=null){
            raf.seek(raf.getFilePointer()-registrySize);
            s.setActive(false);
            write(s);
            System.out.println("Song was deleted");
            return true;
        }
        return false;
    }
    public boolean modifySong(char band, int secondsLong, Song newSong) throws IOException{
        startOfFile();
        Song s = searchByBandAndLength(band,secondsLong);
        if(s!=null){
            raf.seek(raf.getFilePointer()-registrySize);
            s.setBand(newSong.getBand());
            s.setSecondsLong(newSong.getSecondsLong());
            s.setYearReleased(newSong.getYearReleased());
            s.setActive(newSong.isActive());
            write(s);
            System.out.println("Song was modify");
            return true;
        }
        return false;
    }

}
