package main.TP8;

import main.TP7.DoubleNode;

/**
 * Created by JoseRojas on 22/4/17.
 */
public class SearchBinaryTree<T>{
    private DoubleNode<T> root;
    private SearchBinaryTree<T> auxSearchBinaryTree;

    public SearchBinaryTree(){
        root=new DoubleNode<>();
    }

    public boolean contains(Comparable<T> o){
        DoubleNode<T> auxNode= root;
        boolean result;
        if(root == null)
            result=false;
        if(o.compareTo(root.element) == 0)
            result=true;
        else if(o.compareTo(root.element) > 0){
            if(root.right!=null) {
                root = root.right;
                result = contains(o);
            }
            else
                result=false;
        }
        else{
            if(root.left!=null) {
                root = root.left;
                result = contains(o);
            }
            else
                result= false;
        }
        root=auxNode;
        return result;
    }

    public DoubleNode<T> getMin(){
        DoubleNode<T> auxNode= root;
        DoubleNode<T> result= null;

        if(root.left==null)
            result=root;
        if(root.left!=null){
            root=root.left;
            result=getMin();
        }

        root=auxNode;
        return result;
    }

    public DoubleNode<T> getMax(){
        DoubleNode<T> auxNode= root;
        DoubleNode<T> result= null;

        if(root.right==null)
            result=root;
        if(root.right!=null){
            root=root.right;
            result=getMax();
        }

        root=auxNode;
        return result;
    }

    public DoubleNode<T> search(Comparable<T> o){
        DoubleNode<T> auxNode= root;
        DoubleNode<T> result;
        if(root == null)
            result=null;
        if(o.compareTo(root.element) == 0)
            result=root;
        else if(o.compareTo(root.element) > 0){
            root=root.right;
            result=search(o);
        }
        else{
            root=root.left;
            result=search(o);
        }
        root=auxNode;
        return result;
    }

    public void add(Comparable<T> o){
        DoubleNode<T> auxNode= root;
        if(root.element == null || o.compareTo(root.element)==0)
            root.element= (T) o;
        else if(o.compareTo(root.element) > 0){
            if(root.right!= null) {
                root = root.right;
                add(o);
            }
            else{
                root.right=new DoubleNode();
                add(o);
            }
        }
        else{
            if(root.left!=null) {
                root = root.left;
                add(o);
            }
            else {
                root.left = new DoubleNode();
                add(o);
            }
        }
        root=auxNode;
    }

    public void remove(Comparable<T> o){
        root= remove(root, o);
    }

    private DoubleNode<T> remove(DoubleNode<T> iRoot, Comparable<T> o){
        if (o.compareTo(iRoot.element) < 0)
            iRoot.left = remove(iRoot.left, o);
        else if (o.compareTo(iRoot.element) > 0)
            iRoot.right = remove(iRoot.right, o);
        else {
            if (iRoot.left != null && iRoot.right != null) {
                iRoot.element = getMin().element;
                iRoot.right = removeMin(iRoot.right);
            } else if (iRoot.left != null)
                iRoot = iRoot.left;
            else
                iRoot = iRoot.right;
        }
        return iRoot;
    }

    private DoubleNode<T> removeMin(DoubleNode<T> iRoot){
        if(iRoot.left!= null)
            iRoot.left=removeMin(iRoot.left);
        else
            iRoot=iRoot.right;
        return iRoot;
    }
}
