package main.TP8;

/**
 * Created by JoseRojas on 23/4/17.
 */
public class Lamps implements Comparable<Lamps>{
    private String lampCode;
    private int watts;
    private String typeLamp;
    private int lot;

    public Lamps(String lampCode, int watts, String typeLamp) {
        this.lampCode = lampCode.toLowerCase();
        this.watts = watts;
        this.typeLamp = typeLamp.toLowerCase();
        lot = 0;
    }

    public void addLamp(int quantity){
        lot+=quantity;
    }

    public void removeLamp(int quantity){
        lot-=quantity;
    }

    public String getLampCode() {
        return lampCode;
    }

    public int getWatts() {
        return watts;
    }

    public String getTypeLamp() {
        return typeLamp;
    }

    public int getLot() {
        return lot;
    }

    @Override
    public int compareTo(Lamps o) {
        if(lampCode.hashCode() > o.getLampCode().hashCode())
            return 1;
        if(lampCode.hashCode() < o.getLampCode().hashCode())
            return -1;
        else
            return 0;
    }
}
