package main.TP8;

import main.ListTypes.List.StaticList;

/**
 * Created by JoseRojas on 22/4/17.
 */
public class TP8Tester {
    public static void main(String[] args) {
        /*SearchBinaryTree<Integer> searchBinaryTree= new SearchBinaryTree<>();

        searchBinaryTree.add(8);
        searchBinaryTree.add(15);
        searchBinaryTree.add(3);
        searchBinaryTree.add(7);
        searchBinaryTree.add(5);
        searchBinaryTree.add(1);
        searchBinaryTree.add(13);
        searchBinaryTree.add(9);
        searchBinaryTree.add(12);
        searchBinaryTree.add(8);

        System.out.println(searchBinaryTree.contains(5));

        searchBinaryTree.remove(9);
        //searchBinaryTree.add(9);

        System.out.println(searchBinaryTree.contains(9));

        System.out.println(searchBinaryTree.search(13).element);

        System.out.println(searchBinaryTree.getMin().element);

        System.out.println(searchBinaryTree.getMax().element);*/

        StaticList<Lamps> lamps= new StaticList<>();
        Lamps lamps1= new Lamps("ln846", 220, "fgkwkyag");

        lamps.insertNext(lamps1);
        lamps.insertNext(new Lamps("fe048", 220, "fgkwkyag"));
        lamps.insertNext(new Lamps("kg109", 220, "fgkwkyag"));
        lamps.insertNext(new Lamps("js341", 220, "fgkwkyag"));
        lamps.insertNext(new Lamps("ae236", 220, "fgkwkyag"));
        lamps.insertNext(new Lamps("wt004", 220, "fgkwkyag"));

        SearchBinaryTree<Lamps> searchBinaryTree= ReOrder.reOrder(lamps);

        System.out.println(searchBinaryTree.search(lamps1).element.getLampCode());

        searchBinaryTree.search(lamps1).element.addLamp(54);

        System.out.println(searchBinaryTree.search(lamps1).element.getLot());

    }
}
