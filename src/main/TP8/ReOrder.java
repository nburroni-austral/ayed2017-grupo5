package main.TP8;

import struct.istruct.list.List;

/**
 * Created by JoseRojas on 23/4/17.
 */
public class ReOrder {
    public static SearchBinaryTree<Lamps> reOrder(List<Lamps> lampsList){
        SearchBinaryTree<Lamps> tree= new SearchBinaryTree<>();
        lampsList.goTo((lampsList.size()/2)-1);
        tree.add(lampsList.getActual());

        int i=0;
        while (i!=(lampsList.size()-1)){
            lampsList.goTo(i);
            tree.add(lampsList.getActual());
            i++;
        }

        return tree;
    }
}
