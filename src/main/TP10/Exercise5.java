package main.TP10;


import java.util.Scanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Exercise5 {

    public static void main(String[] args) {
        modifiedCountryFileSorter();
    }

    public static void modifiedCountryFileSorter(){

        try{
            Scanner scanner = new Scanner(System.in);
            System.out.println("Escriba el nombre del archivo que desea sortear: ");
            String fileName = scanner.next();
            System.out.println("Escriba el numero de poblacion a utilizar como criterio: ");
            Double populationNumber = scanner.nextDouble();
            System.out.println("Desea escrbir PBI, POB o ambas?");
            String instruction = scanner.next();
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            FileWriter fr1 = new FileWriter(fileName + "Under" + populationNumber + "mill");
            FileWriter fr2 = new FileWriter(fileName + "Others");
            String[] fields;
            String country;
            Double population;
            String pbi;
            String line = br.readLine();
            while (line != null){
                fields = line.split(" ");
                country = fields[0];
                population = Double.parseDouble(fields[1]);
                pbi = fields[2];
                if (instruction.toUpperCase().equals("PBI")){
                    if (population < populationNumber){
                        fr1.write(country + " " + pbi + "\n");
                    } else {
                        fr2.write(country + " " + pbi + "\n");
                    }
                } else if (instruction.toUpperCase().equals("POB")){
                    if (population < populationNumber){
                        fr1.write(country + " " + population + "\n");
                    } else {
                        fr2.write(country + " " + population + "\n");
                    }
                } else {
                    if (population < populationNumber){
                        fr1.write(line + "\n");
                    } else {
                        fr2.write(line + "\n");
                    }
                }
                line = br.readLine();
            }
            fr1.close();
            fr2.close();
            br.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
