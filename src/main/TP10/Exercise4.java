package main.TP10;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Exercise4 {

    public static void main(String[] args) {
        countryFileSorter();
    }

    public static void countryFileSorter(){
        try{
            Scanner scanner = new Scanner(System.in);
            System.out.println("Escriba el nombre del archivo que desea sortear: ");
            String fileName = scanner.next();
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            FileWriter fr1 = new FileWriter(fileName + "Under30mill");
            FileWriter fr2 = new FileWriter(fileName + "Others");
            String[] fields;
            Double population;
            String line = br.readLine();
            while (line != null){
                fields = line.split(" ");
                population = Double.parseDouble(fields[1]);
                if (population < 30){
                    fr1.write(line + "\n");
                } else {
                    fr2.write(line + "\n");
                }
                line = br.readLine();
            }
            fr1.close();
            fr2.close();
            br.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }


}
