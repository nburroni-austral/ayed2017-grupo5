package main.TP10;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Exercise2 {

    public static void main(String[] args) {
        countCharTimes('d', "C:\\Users\\Tomi\\Documents\\Facultad\\AyED\\TPs\\ayed2017-grupo5\\src\\main\\TP10\\Ex1&2Test");
    }

    public static int countCharTimes(char character, String fileName){
        int counter = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();
            while (line != null){
                for (int i = 0; i < line.length(); i++){
                    if (line.charAt(i) == character){
                        counter++;
                    }
                }
                line = br.readLine();
            }
            br.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        System.out.println("El numero de veces que se encuentra el caracter es: " + counter);
        return counter;
    }
}
