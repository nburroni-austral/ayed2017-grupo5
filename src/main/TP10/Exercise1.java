package main.TP10;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class Exercise1 {

    public static void main(String[] args) {
        analyzeTextFile();
    }

    public static void analyzeTextFile(){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese C, L o ambas");
        String instructions = scanner.nextLine();
        System.out.println("Ingrese el nombre del archivo que desea analizar");
        String fileName = scanner.next();
        if (instructions.equals("C")){
            System.out.println("El numero de caracteres es: " + countChars(fileName));
        } else if (instructions.equals("L")){
            System.out.println("El numero de lineas es: " + countLines(fileName));
        } else {
            System.out.println("El numero de caracteres es: " + countChars(fileName));
            System.out.println("El numero de lineas es: " + countLines(fileName));
        }
    }

    private static int countChars(String fileName){
        int counter = 0;
        try{
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();
            while (line != null) {
                for (int i = 0; i < line.length(); i++){
                    if (line.charAt(i) != ' '){
                        counter++;
                    }
                }
                line = br.readLine();
            }
            br.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        return counter;
    }

    private static int countLines(String fileName){
        int counter = 0;
        try{
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String line = br.readLine();
            while (line != null){
                counter++;
                line = br.readLine();
            }
            br.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        return counter;
    }



}
