package main.TP10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
public class Exercise3 {

    public static void main(String[] args) {
        convertTextFile();
    }

    public static void convertTextFile(){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Escriba el nombre del archivo que desea convertir: ");
        String convertFileName = scanner.next();
        String newFileName = convertFileName + "Converted";
        System.out.println("Desea convertir el texto a mayuscula o minuscula?");
        String instruction = scanner.next();
        if (instruction.toLowerCase().equals("mayuscula")){
            fileToUpperCase(convertFileName, newFileName);
        } else if (instruction.toLowerCase().equals("minuscula")){
            fileToLowerCase(convertFileName, newFileName);
        }
    }

    private static void fileToLowerCase(String convertFileName, String newFileName){
        try{
            BufferedReader br = new BufferedReader(new FileReader(convertFileName));
            FileWriter fw = new FileWriter(newFileName);
            String line = br.readLine();
            while (line != null){
                fw.write(line.toLowerCase() + "\n");
                line = br.readLine();
            }
            fw.close();
            br.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static void fileToUpperCase(String convertFileName, String newFileName){
        try{
            BufferedReader br = new BufferedReader(new FileReader(convertFileName));
            FileWriter fw = new FileWriter(newFileName);
            String line = br.readLine();
            while (line != null){
                fw.write(line.toUpperCase() + "\n");
                line = br.readLine();
            }
            fw.close();
            br.close();
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
