package main.TP7;


import main.TP3.Stack.StaticStack;

public class ShowTreeMethods {


    public static void showTreeInPreorder(BinaryTree tree){
        if (tree.isEmpty()){
            return;
        } else if (!(tree.hasLeft() && tree.hasRight())){
            System.out.println(tree.getRoot());
            return;
        } else if (tree.hasLeft() && !tree.hasRight()){
            System.out.println(tree.getRoot());
            showTreeInPreorder(tree.getLeft());
            return;
        } else if (tree.hasRight() && !tree.hasLeft()){
            System.out.println(tree.getRoot());
            showTreeInPreorder(tree.getRight());
            return;
        } else {
            System.out.println(tree.getRoot());
            showTreeInPreorder(tree.getLeft());
            showTreeInPreorder(tree.getRight());
            return;
        }
    }


    public static void showTreeInInorder(BinaryTree tree){
        if (tree.isEmpty()){
            System.out.println("El árbol está vacío");
            return;
        } else if (!(tree.hasLeft() && tree.hasRight())){
            System.out.println(tree.getRoot());
            return;
        } else if (tree.hasLeft() && !tree.hasRight()){
            showTreeInInorder(tree.getLeft());
            System.out.println(tree.getRoot());
            return;
        } else if (tree.hasRight() && !tree.hasLeft()){
            System.out.println(tree.getRoot());
            showTreeInInorder(tree.getRight());
            return;
        } else {
            showTreeInInorder(tree.getLeft());
            System.out.println(tree.getRoot());
            showTreeInInorder(tree.getRight());
            return;
        }
    }

    public static void showTreeInPostorder(BinaryTree tree){
        if (tree.isEmpty()){
            System.out.println("El árbol está vacío");
            return;
        } else if (!(tree.hasLeft() && tree.hasRight())){
            System.out.println(tree.getRoot());
            return;
        } else if (tree.hasLeft() && !tree.hasRight()){
            showTreeInPostorder(tree.getLeft());
            System.out.println(tree.getRoot());
            return;
        } else if (tree.hasRight() && !tree.hasLeft()){
            showTreeInPostorder(tree.getRight());
            System.out.println(tree.getRoot());
            return;
        } else {
            showTreeInPostorder(tree.getLeft());
            showTreeInPostorder(tree.getRight());
            System.out.println(tree.getRoot());
            return;
        }
    }

    public static void showTreeByLevels(BinaryTree tree){
        if (tree.isEmpty()){
            System.out.println("El árbol está vacío");
            return;
        }
        StaticStack<BinaryTree> rootsStack = new StaticStack<BinaryTree>();
        StaticStack<BinaryTree> auxStack = new StaticStack<BinaryTree>();
        rootsStack.push(tree);
        for (int i = 0; i <= tree.getHeight(); i++){
            while (!rootsStack.isEmpty()){
                System.out.println(rootsStack.peek().getRoot());
                if (rootsStack.peek().hasLeft()){
                    auxStack.push(rootsStack.peek().getLeft());
                }
                if (rootsStack.peek().hasRight()){
                    auxStack.push(rootsStack.peek().getRight());
                }
                rootsStack.pop();
            }
            while (!auxStack.isEmpty()){
                rootsStack.push(auxStack.peek());
                auxStack.pop();
            }
        }
    }
}
