package main.TP7;


import java.util.ArrayList;

public class Exercise14 {

    public static int getSum(BinaryTree<Integer> tree){
        if (tree.isEmpty()){
            return 0;
        } else {
            return tree.getRoot() + getSum(tree.getLeft()) + getSum(tree.getRight());
        }
    }

    public static int getSumOfDivisiblesBy3(BinaryTree<Integer> tree){
        if (tree.isEmpty()){
            return 0;
        } else {
            if (tree.getRoot() % 3 == 0){
                return tree.getRoot() + getSumOfDivisiblesBy3(tree.getLeft()) + getSumOfDivisiblesBy3(tree.getRight());
            } else {
                return getSumOfDivisiblesBy3(tree.getLeft()) + getSumOfDivisiblesBy3(tree.getRight());
            }
        }
    }


    public static boolean iguales(BinaryTree tree1, BinaryTree tree2){
        if (tree1.isEmpty() && tree2.isEmpty()){
            return true;
        }
        if (!tree1.isEmpty() && !tree2.isEmpty()){
            return tree1.getRoot().equals(tree2.getRoot()) && iguales(tree1.getLeft(), tree2.getLeft()) && iguales(tree1.getRight(), tree2.getRight());
        }
        return false;
    }

    public static boolean isomorfos(BinaryTree tree1, BinaryTree tree2){
        if (iguales(tree1, tree2)){
            return false;
        }
        if (tree1.isEmpty() != tree2.isEmpty() || tree1.hasLeft() != tree2.hasLeft() || tree1.hasRight() != tree2.hasRight()){
            return false;
        }
        if (tree1.hasLeft()){
            if (!isomorfos(tree1.getLeft(), tree2.getLeft())){
                return false;
            }
        }
        if (tree1.hasRight()){
            if (!isomorfos(tree1.getRight(), tree2.getRight())){
                return false;
            }
        }
        return true;
    }

    public static boolean semejantes(BinaryTree tree1, BinaryTree tree2){
        if (iguales(tree1, tree2) || isomorfos(tree1, tree2)){
            return false;
        }
        if (tree1.isEmpty() && tree2.isEmpty()){
            return true;
        }
        if (!(tree2.getTimesInTree(tree1.getRoot()) == 1)){
            return false;
        }
        if (tree1.hasLeft()){
            if (!semejantes(tree1.getLeft(), tree2)){
                return false;
            }
        }
        if (tree1.hasRight()){
            if (!semejantes(tree1.getRight(), tree2)){
                return false;
            }
        }

        return true;
    }


    public static boolean completo(BinaryTree tree){
        if (tree.isEmpty()){
            return false;
        } else if (!(tree.hasLeft() && tree.hasRight())){
            return true;
        } else if (tree.hasLeft() && !tree.hasRight()){
            return false;
        } else if (tree.hasRight() && !tree.hasLeft()){
            return false;
        } else {
            return completo(tree.getLeft()) && completo(tree.getRight());
        }
    }

    public static boolean lleno(BinaryTree tree){
        if (!completo(tree)){
            return false;
        }
        return tree.getNumberOfElementsInLevel(tree.getHeight() + 1) == Math.pow(2, tree.getHeight());
    }

    public static boolean estable(BinaryTree<Integer> tree){
        if (tree.isEmpty() || (!(tree.hasLeft() && tree.hasRight()))){
            return true;
        }
        if (tree.getRoot() <= (Integer) tree.getLeft().getRoot()) {
            return false;
        }
        if (tree.getRoot() <= (Integer) tree.getRight().getRoot()) {
            return false;
        }
        return estable(tree.getLeft()) && estable(tree.getRight());
    }

    public static boolean ocurreArbin(BinaryTree tree1, BinaryTree tree2){
        if (tree2.isEmpty()){
            return true;
        }
        if (tree1.isEmpty()){
            return false;
        }
        if (iguales(tree1, tree2)){
            return true;
        }

        return ocurreArbin(tree1.getLeft(), tree2) || ocurreArbin(tree1.getRight(), tree2);
    }

    public static void mostrarFrontera(BinaryTree tree){
        if (tree.isEmpty()){
            System.out.println("El árbol está vacío");
        } else if (!(tree.hasLeft() && tree.hasRight())){
            System.out.println(tree.getRoot());
        } else if (tree.hasLeft() && !tree.hasRight()){
            mostrarFrontera(tree.getLeft());
        } else if (tree.hasRight() && !tree.hasLeft()){
            mostrarFrontera(tree.getRight());
        } else {
            mostrarFrontera(tree.getLeft());
            mostrarFrontera(tree.getRight());
        }

    }

    public static ArrayList<Object> frontera(BinaryTree tree){
        ArrayList<Object> frontera = new ArrayList<>();
        if (tree.isEmpty()){
            return frontera;
        } else if (!(tree.hasLeft() && tree.hasRight())){
            frontera.add(tree.getRoot());
        } else if (tree.hasLeft() && !tree.hasRight()){
            frontera.addAll(frontera(tree.getLeft()));
        } else if (tree.hasRight() && !tree.hasLeft()){
            frontera.addAll(frontera(tree.getRight()));
        } else {
            frontera.addAll(frontera(tree.getLeft()));
            frontera.addAll(frontera(tree.getRight()));
        }
        return frontera;
    }

}
