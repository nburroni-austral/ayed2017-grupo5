package main.TP7;


//import com.sun.prism.es2.ES2Graphics;

import java.util.ArrayList;

public class Tester {
    public static void main(String[] args) {
        BinaryTree<Integer> aLL = new BinaryTree<>(4);
        BinaryTree<Integer> aLR = new BinaryTree<>(12);
        BinaryTree<Integer> aRL = new BinaryTree<>(18);
        BinaryTree<Integer> aRR = new BinaryTree<>(24);
        BinaryTree<Integer> bLL = new BinaryTree<>(31);
        BinaryTree<Integer> bLR = new BinaryTree<>(44);
        BinaryTree<Integer> bRL = new BinaryTree<>(66);
        BinaryTree<Integer> bRR = new BinaryTree<>(90);
        BinaryTree<Integer> aL = new BinaryTree<>(10, aLL, aLR);
        BinaryTree<Integer> aR = new BinaryTree<>(22, aRL, aRR);
        BinaryTree<Integer> bL = new BinaryTree<>(35, bLL, bLR);
        BinaryTree<Integer> bR = new BinaryTree<>(70, bRL, bRR);
        BinaryTree<Integer> a = new BinaryTree<>(15, aL, aR);
        BinaryTree<Integer> b = new BinaryTree<>(50, bL, bR);
        BinaryTree<Integer> testTree = new BinaryTree<>(25, a, b);
        BinaryTree<Integer> testTree2 = new BinaryTree<>(25, b, a);

        System.out.println(testTree.isEmpty());
        System.out.println(testTree.getLeft().getLeft().getLeft().hasLeft());
        System.out.println(testTree.getLeft().getLeft().getLeft().hasRight());
        System.out.println(testTree.hasLeft());
        System.out.println(testTree.getRoot());
        System.out.println(testTree.getLeft().getRoot());
        System.out.println(testTree.getRight().getRoot());
        System.out.println(testTree.getWeight());
        System.out.println(testTree.getNumberOfLeaves());
        System.out.println(testTree.getTimesInTree(70));
        System.out.println(testTree.getNumberOfElementsInLevel(3));
        System.out.println(testTree.getHeight());
        System.out.println(Exercise14.getSum(testTree));
        System.out.println(Exercise14.getSumOfDivisiblesBy3(testTree));
        System.out.println(Exercise14.iguales(testTree,testTree));
        System.out.println(Exercise14.iguales(testTree,testTree2));
        System.out.println(Exercise14.isomorfos(testTree,testTree2));
        System.out.println(Exercise14.isomorfos(testTree, a));
        System.out.println(Exercise14.semejantes(testTree, testTree2));
        System.out.println(Exercise14.completo(testTree));
        System.out.println(Exercise14.lleno(testTree));
        BinaryTree<Integer> c = new BinaryTree<>(20);
        BinaryTree<Integer> d = new BinaryTree<>(25);
        BinaryTree<Integer> e = new BinaryTree<>(30, c, d);
        System.out.println(Exercise14.estable(e));
        System.out.println(Exercise14.estable(testTree));
        System.out.println(Exercise14.ocurreArbin(testTree, a));
        System.out.println("Frontera: ");
        Exercise14.mostrarFrontera(testTree);
        ArrayList<Object> frontera = Exercise14.frontera(testTree);
        System.out.println("Arraylist frontera: ");
        for (Object obj : frontera){
            System.out.println(obj);
        }

        System.out.println("El árbol en preorden: ");
        ShowTreeMethods.showTreeInPreorder(testTree);
        System.out.println("El árbol en inorden: ");
        ShowTreeMethods.showTreeInInorder(testTree);
        System.out.println("El árbol en postorden: ");
        ShowTreeMethods.showTreeInPostorder(testTree);
        System.out.println("El árbol por niveles: ");
        ShowTreeMethods.showTreeByLevels(testTree);

        testTree.saveInDisk();
        BinaryTree<Integer> treeFromDisk = BinaryTree.readFromDisk("c:\\temp\\address.ser");
        System.out.println("El árbol por niveles: ");
        ShowTreeMethods.showTreeByLevels(treeFromDisk);
        System.out.println(Exercise14.iguales(testTree, treeFromDisk));

    }
}
