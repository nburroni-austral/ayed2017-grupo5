package main.TP7;


import main.TP3.Stack.StaticStack;

import java.io.*;

public class BinaryTree<T> implements struct.istruct.BinaryTree, Serializable{

    private DoubleNode<T> root;

    public BinaryTree(){
        root = null;
    }

    public BinaryTree(T root){
        this.root = new DoubleNode<>();
        this.root.element = root;
        this.root.left = null;
        this.root.right = null;
    }

    public BinaryTree(T root, BinaryTree left, BinaryTree right){
        this.root = new DoubleNode<>();
        this.root.element = root;
        this.root.left = left.root;
        this.root.right = right.root;
    }


    public boolean isEmpty() {
        return (root == null);
    }

    public boolean hasLeft(){
        return !(root.left == null);
    }

    public boolean hasRight(){
        return !(root.right == null);
    }

    public T getRoot() {
        return root.element;
    }


    public BinaryTree getLeft() {
        BinaryTree<T> leftLeaf = new BinaryTree();
        leftLeaf.root = root.left;
        return leftLeaf;
    }


    public BinaryTree getRight() {
        BinaryTree<T> rightLeaf = new BinaryTree();
        rightLeaf.root = root.right;
        return rightLeaf;
    }


    //Exercise 13

    public int getWeight(){
        if (isEmpty()){
            return 0;
        } else if (!(hasLeft() && hasRight())){
            return 1;
        } else if (hasLeft() && !hasRight()){
            return 1 + getLeft().getWeight();
        } else if (hasRight() && !hasLeft()){
            return 1 + getRight().getWeight();
        } else {
            return 1 + getLeft().getWeight() + getRight().getWeight();
        }
    }

    public int getNumberOfLeaves(){
        if (isEmpty()) {
            return 0;
        } else if (!(hasLeft() && hasRight())){
            return 1;
        } else if (hasLeft() && !hasRight()){
            return getLeft().getNumberOfLeaves();
        } else if (hasRight() && !hasLeft()){
            return getRight().getNumberOfLeaves();
        } else {
            return getLeft().getNumberOfLeaves() + getRight().getNumberOfLeaves();
        }
    }

    public int getTimesInTree(T objectToLookFor){
        if (isEmpty()){
            return 0;
        } else if (!(hasLeft() && hasRight())){
            if (getRoot() == objectToLookFor){
                return 1;
            }
            return 0;
        } else if (hasLeft() && !hasRight()){
            if (getRoot() == objectToLookFor){
                return 1 + getLeft().getTimesInTree(objectToLookFor);
            }
            return getLeft().getTimesInTree(objectToLookFor);
        } else if (hasRight() && !hasLeft()){
            if (getRoot() == objectToLookFor){
                return 1 + getRight().getTimesInTree(objectToLookFor);
            }
            return getRight().getTimesInTree(objectToLookFor);
        } else {
            if (getRoot() == objectToLookFor){
                return 1 + getLeft().getTimesInTree(objectToLookFor) + getRight().getTimesInTree(objectToLookFor);
            }
            return getLeft().getTimesInTree(objectToLookFor) + getRight().getTimesInTree(objectToLookFor);        }
    }

    public int getNumberOfElementsInLevel(int level){
        if (level > getHeight() + 1 || isEmpty()){
            return 0;
        }
        if (level == 1){
            return 1;
        }
        StaticStack<BinaryTree> rootsStack = new StaticStack<BinaryTree>();
        StaticStack<BinaryTree> auxStack = new StaticStack<BinaryTree>();
        rootsStack.push(this);
        for (int i = 1; i < level; i++){
            while (!rootsStack.isEmpty()){
                if (rootsStack.peek().hasLeft()){
                    auxStack.push(getLeft());
                }
                if (rootsStack.peek().hasRight()){
                    auxStack.push(getRight());
                }
                rootsStack.pop();
            }
            while (!auxStack.isEmpty()){
                rootsStack.push(auxStack.peek());
                auxStack.pop();
            }
        }
        return rootsStack.size();
    }

    public int getHeight(){
        if (isEmpty() || (!(hasLeft() && hasRight()))) {
            return 0;
        } else if (hasLeft() && !hasRight()){
            return 1 + getLeft().getHeight();
        } else if (hasRight() && !hasLeft()){
            return 1 + getRight().getHeight();
        } else {
            if (getLeft().getHeight() >= getRight().getHeight()){
                return 1 + getLeft().getHeight();
            } else {
                return 1 + getRight().getHeight();
            }
        }
    }

    public void saveInDisk(){
        FileOutputStream fout = null;
        ObjectOutputStream oos = null;
        try {
            fout = new FileOutputStream("c:\\temp\\address.ser");
            oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static BinaryTree readFromDisk(String fileName){
        FileInputStream fin = null;
        ObjectInputStream ois = null;
        BinaryTree result = null;
        try {
            fin = new FileInputStream(fileName);
            ois = new ObjectInputStream(fin);
            result = (BinaryTree) ois.readObject();
        } catch (Exception ex){
            ex.printStackTrace();
        } finally {
            if (fin != null){
                try {
                    fin.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
            if (ois != null){
                try {
                    ois.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

}
