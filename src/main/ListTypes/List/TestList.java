package main.ListTypes.List;

import main.ListTypes.List.StaticList;

import java.util.Scanner;

/**
 * Created by JoseRojas on 19/4/17.
 */
public class TestList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StaticList<Integer> staticList = new StaticList<>();

        int a = scanner.nextInt();
        int b=0;
        while (a!=0){
            b+=a;
            staticList.insertNext(a);
            a=scanner.nextInt();
        }

        staticList.goTo(staticList.size()/2);
        System.out.println(staticList.getActual());

        /*StaticList<Integer> aux= staticList;

        int aa=0;
        while(!aux.isVoid()){
            //System.out.println(staticList.getActual());
            aa+= aux.getActual();
            aux.remove();
        }

        if(aa == b)
            System.out.println(b);*/
    }
}
