package main.ListTypes.SortedList;

/**
 * Created by JoseRojas on 12/5/17.
 */
public class SLTest {
    public static void main(String[] args) {
        StaticSortedList<Integer> sortedList= new StaticSortedList<>(Integer.class);

        sortedList.insert(5);
        sortedList.insert(25);
        sortedList.insert(55);
        sortedList.insert(15);
        sortedList.insert(5);
        sortedList.insert(1);
        sortedList.insert(10);
        sortedList.insert(95);
        sortedList.insert(45);
        sortedList.insert(21);
        sortedList.insert(0);

        for(int i=0; i<sortedList.size(); i++){
            sortedList.goTo(i);
            System.out.println(sortedList.getActual());
        }
    }
}
