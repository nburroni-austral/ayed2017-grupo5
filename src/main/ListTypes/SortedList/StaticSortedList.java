package main.ListTypes.SortedList;

import struct.istruct.list.GeneralList;
import struct.istruct.list.SortedList;

import java.lang.reflect.Array;

/**
 * Created by JoseRojas on 11/5/17.
 */
public class StaticSortedList<T extends Comparable<T>> implements SortedList<T> {
    private static final int DEFAULT_CAPACITY = 25;
    private T[] sortedList;
    private int window;
    private int size;
    private Class<T> tClass;

    public StaticSortedList(Class<T> tClass,int capacity){
        this.tClass=tClass;
        sortedList= (T[]) Array.newInstance(tClass, capacity);
        window=0;
        size=0;
    }

    public StaticSortedList(Class<T> tClass){
        this(tClass ,DEFAULT_CAPACITY);
    }

    public StaticSortedList(int window, int size, T[] objects){
        this.window=window;
        this.size=size;
        sortedList=objects;
    }

    @Override
    public void remove() {
        for (int i = window; i < sortedList.length - 1; i++)
            sortedList[i] = sortedList[i + 1];
        size--;
        if (window >= size) window = size - 1;
    }

    @Override
    public void goNext(){
        if (window == size - 1)
            throw new IndexOutOfBoundsException("Reached the end of the list");
        window++;
    }

    @Override
    public void goPrev() {
        if (window == 0)
            throw new IndexOutOfBoundsException("Reached the beginning of the list");
        window--;
    }

    @Override
    public void goTo(int n) {
        if (n < 0 || n >= sortedList.length)
            throw new IndexOutOfBoundsException("There is no such index in this list");
        window = n;
    }

    @Override
    public T getActual() {
        if (isVoid())
            throw new NullPointerException("The list is empty");
        return (T) sortedList[window];
    }

    @Override
    public int getActualPosition() {
        return window;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isVoid() {
        return sortedList[0] == null;
    }

    @Override
    public boolean endList() {
        return window == sortedList.length - 1;
    }

    @Override
    public GeneralList<T> clone() {
        T[] cloned = (T[]) Array.newInstance(tClass, sortedList.length);
        for (int i = 0; i < sortedList.length; i++)
            cloned[i] = sortedList[i];
        return new StaticSortedList<T>(window, size, cloned);
    }

    @Override
    public void insert(T obj) {
        if (size == sortedList.length)
            grow();
        for(int i=0; i<sortedList.length; i++) {
            if (sortedList[i] == null) {
                sortedList[i]=obj;
                size++;
                break;
            }
            int jash= obj.compareTo(sortedList[i]);
            if (obj.compareTo(sortedList[i]) < 0 || obj.compareTo(sortedList[i]) == 0) {
                for (int j = sortedList.length - 1; j > i; j--)
                    sortedList[j] = sortedList[j - 1];
                sortedList[i] = obj;
                size++;
                break;
            }
        }
    }

    private void grow() {
        T[] tempObjects = (T[]) Array.newInstance(tClass, sortedList.length+DEFAULT_CAPACITY);
        for (int i = 0; i < sortedList.length; i++)
            tempObjects[i] = sortedList[i];
        sortedList = tempObjects;
    }
}
