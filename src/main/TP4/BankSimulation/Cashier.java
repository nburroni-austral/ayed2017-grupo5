package main.TP4.BankSimulation;

import java.util.Random;

/**
 * Created by JoseRojas on 11/4/17.
 */
public class Cashier {
    private int delay;
    private boolean free;
    private long inCostumer;

    private int min;
    private int max;

    private Client client;

    public Cashier(int min, int max, Client client) {
        this.max=max;
        this.min=min;
        this.client=client;
        free = true;
    }

    public void enterCostumer(Client c){
        Random rdm= new Random();
        delay= rdm.nextInt((max-min+1))+ min;
        inCostumer= System.currentTimeMillis();
        free= false;
        client=c;
    }

    public boolean isFree(){
        long end = System.currentTimeMillis();
        if((delay<=(end-inCostumer)) && !free) {
            free = true;
            client.finish();
            client=null;
        }
        return free;
    }
}
