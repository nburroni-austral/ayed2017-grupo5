package main.TP4.BankSimulation;

import main.TP4.Queue.StaticQueue;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by JoseRojas on 11/4/17.
 */
public class StrategyA {
    private Bank bank;
    private StaticQueue<Client> line;
    private ArrayList<Client> history= new ArrayList<>();
    private long timesAddClients;
    private long auxTime;

    public StrategyA(){
        bank= new Bank();
        line= new StaticQueue<>(99999);
    }

    public void inicialitation(){
        timesAddClients= System.currentTimeMillis();
        auxTime=0;
        bank.open();
        controller();
    }

    private void controller(){
        addClients();
        long timesEndClients= System.currentTimeMillis();
        if((timesEndClients-timesAddClients)>=18000000)
            bank.close();
        check();
        if(line.size()>0){
            selection();
            controller();
        }
   }

    private void addClients(){
        long endAdd= System.currentTimeMillis();
        if((15000 <= endAdd-auxTime && bank.isOpen()) || auxTime==0 ){
            auxTime=endAdd;
            Random rdm= new Random();
            int total= rdm.nextInt(11);
            for(int i=0; i<total; i++){
                Client client= new Client(line.size());
                if(client.isEnter()) {
                    line.enqueue(client);
                    history.add(client);
                }
            }
        }
    }

    private void check(){
        bank.getCashier1().isFree();
        bank.getCashier2().isFree();
        bank.getCashier3().isFree();
    }

    private void selection(){
        if(bank.getCashier1().isFree())
            bank.getCashier1().enterCostumer(line.dequeue());
        if(bank.getCashier2().isFree())
            bank.getCashier2().enterCostumer(line.dequeue());
        if(bank.getCashier3().isFree())
            bank.getCashier3().enterCostumer(line.dequeue());
    }

    public ArrayList<Client> getHistory() {
        return history;
    }
}
