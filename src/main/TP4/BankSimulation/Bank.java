package main.TP4.BankSimulation;

/**
 * Created by JoseRojas on 11/4/17.
 */
public class Bank {
    private Cashier cashier1;
    private Cashier cashier2;
    private Cashier cashier3;
    private boolean open;

    public Bank(){
        cashier1= new Cashier(500,1500, null);
        cashier2= new Cashier(500,2000, null);
        cashier3= new Cashier(500,2500, null);
        open=false;
    }

    public void open(){
        open=true;
    }

    public void close(){
        open=false;
    }

    public Cashier getCashier1() {
        return cashier1;
    }

    public Cashier getCashier2() {
        return cashier2;
    }

    public Cashier getCashier3() {
        return cashier3;
    }

    public boolean isOpen() {
        return open;
    }
}
