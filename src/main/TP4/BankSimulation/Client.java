package main.TP4.BankSimulation;

import java.util.Random;

/**
 * Created by JoseRojas on 11/4/17.
 */
public class Client {
    private long initTime;
    private long time;
    private long finishTime;
    private boolean isEnter;

    public Client(int amount){
        isEnter=false;
        decide(amount);
    }

    public void finish(){
        finishTime= System.currentTimeMillis();
        time= finishTime-initTime;
    }

    public long getTime() {
        return time;
    }

    private void decide(int amount){
        if(amount>8){
            Random rdm= new Random();
            int index= rdm.nextInt(1);
            if(index==0)
                isEnter= false;
            else {
                initTime= System.currentTimeMillis();
                isEnter= true;
            }
        }
        if(amount>3 && amount<=8){
            Random rdm= new Random();
            int index= rdm.nextInt(3);
            if(index==0)
                isEnter= false;
            else {
                initTime= System.currentTimeMillis();
                isEnter= true;
            }
        }
        else{
            initTime= System.currentTimeMillis();
            isEnter= true;
        }
    }

    public boolean isEnter() {
        return isEnter;
    }
}
