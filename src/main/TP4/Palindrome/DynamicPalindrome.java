package main.TP4.Palindrome;

import java.util.Scanner;
import main.TP3.Stack.DynamicStack;
import main.TP4.Queue.DynamicQueue;

/**
 * Created by JoseRojas on 12/4/17.
 */
public class DynamicPalindrome {
    private String word;
    private DynamicStack<Character> stackWord;
    private DynamicQueue<Character> queueWord;

    /** Create a DynamicPalindrome by prompting the system for a word
     *  and initializing the queue and stack. Also execute the initialization method
     * */
    public DynamicPalindrome(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter word: ");
        word=scanner.nextLine();
        word = word.toLowerCase();
        stackWord= new DynamicStack<>();
        queueWord= new DynamicQueue<>();
        initialization();
    }

    private void initialization(){
        for(int i=0; i<word.length(); i++) {
            stackWord.push(word.charAt(i));
            queueWord.enqueue(word.charAt(i));
        }
    }

    /** Analyze the letters of the extremes and if they are
     *  equal passes to the next two letters that are more in the center
     * */
    public boolean analyze(){
        boolean result= false;
        int i=0;
        while(i!=(word.length()/2)){
            if( (stackWord.peek()) == (queueWord.dequeue()) ){
                stackWord.pop();
                result=true;
                i++;
            }
            else{
                result=false;
                i=word.length()/2;
            }
        }
        return result;
    }
}
