package main.TP4.Palindrome;

import main.TP3.Stack.StaticStack;
import main.TP4.Queue.StaticQueue;

import java.util.Scanner;


public class StaticPalindrome {
    private String word;
    private StaticStack<Character> stackWord;
    private StaticQueue<Character> queueWord;

    /** Create a StaticPalindrome by prompting the system for a word
     *  and initializing the queue and stack. Also execute the initialization method
     * */
    public StaticPalindrome(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter word: ");
        word=scanner.nextLine();
        word = word.toLowerCase();
        stackWord= new StaticStack<>();
        queueWord= new StaticQueue<>();
        initialization();
    }

    private void initialization(){
        for(int i=0; i<word.length(); i++) {
            stackWord.push(word.charAt(i));
            queueWord.enqueue(word.charAt(i));
        }
    }

    /** Analyze the letters of the extremes and if they are
     *  equal passes to the next two letters that are more in the center
     * */
    public boolean analyze(){
        boolean result= false;
        int i=0;
        while(i!=(word.length()/2)){
            if( (stackWord.peek()) == (queueWord.dequeue()) ){
                stackWord.pop();
                result=true;
                i++;
            }
            else{
                result=false;
                i=word.length()/2;
            }
        }
        return result;
    }
}