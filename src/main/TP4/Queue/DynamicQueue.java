package main.TP4.Queue;

import struct.istruct.Queue;


public class DynamicQueue<T> implements Queue<T>{
    private Node<T> front;
    private Node<T> back;
    private int size;

    /** Creates DynamicQueue with null front and back nodes and initializes size to zero
     */
    public DynamicQueue(){
        front = null;
        back = null;
        size = 0;
    }

    /** Add the object to the last place in the queue
     */
    @Override
    public void enqueue(T t) {
        Node<T> node = new Node<T>();
        node.element = t;
        if (isEmpty()){
            front = back = node;
        } else {
            back.next = node;
            back = node;
            back.next = null;
        }
        size++;
    }

    /** Removes the first object from the queue and returns it
     */
    @Override
    public T dequeue() {
        if (isEmpty()){
            //System.out.println("La cola está vacía");
            return null;
        } else {
            Node<T> aux = front;
            front = front.next;
            size--;
            return aux.element;
        }
    }

    /** Returns true if this is empty
     */
    @Override
    public boolean isEmpty() {
        return front == null;
    }

    /** Returns the number of objects in the queue
     */
    @Override
    public int length() {
        return size;
    }

    /** Returns the number of objects in the queue
     */
    @Override
    public int size() {
        return size;
    }

    /** Empty the queue
     */
    @Override
    public void empty() {
        front = back = null;
        size = 0;
    }
}
