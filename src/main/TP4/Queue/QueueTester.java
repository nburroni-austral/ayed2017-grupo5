package main.TP4.Queue;


public class QueueTester {
    public static void main(String[] args){
        DynamicQueue<Integer> queue = new DynamicQueue<>();
        System.out.println(queue.isEmpty());
        queue.enqueue(45);
        queue.enqueue(35);
        queue.enqueue(25);
        queue.enqueue(15);
        queue.enqueue(55);
        queue.enqueue(85);
        queue.enqueue(10);
        queue.dequeue();
        queue.dequeue();
        queue.enqueue(30);
        System.out.println(queue.size());
        while(!queue.isEmpty()){
            System.out.println(queue.dequeue());
        }
    }
}
