package main.TP4.Queue;


public class Node<T> {
    public T element;
    public Node next;

    public Node() {
        element = null;
        next = null;
    }
}
