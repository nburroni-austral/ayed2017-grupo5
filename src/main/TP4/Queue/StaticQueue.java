package main.TP4.Queue;

import struct.istruct.Queue;



public class StaticQueue<T> implements Queue<T>{
    private T[] queue;
    private int front;
    private int back;
    private T[] clone;

    /** Create a StatickQueue with a length of 50
     */
    public StaticQueue(){
        queue= (T[]) new Object[50];
        clone= (T[]) new Object[50];
        front=0;
        back=0;

    }

    /** Create a StatickQueue with the length that you pass
     */
    public StaticQueue(int limit){
        queue= (T[]) new Object[limit];
        clone= (T[]) new Object[limit];
        front=0;
        back=0;
    }

    /** Add the object to the last place in the queue
     */
    @Override
    public void enqueue(T t) {
        if(back>=queue.length)
            System.out.println("No hay mas lugar, no lo agrega");
        else {
            queue[back] = t;
            back++;
        }
    }

    /** Removes the first object from the queue and returns it
     */
    @Override
    public T dequeue() {
        T t = null;
        if(back!=0) {
            t=queue[front];
            queue[front] = null;
            back--;
            reOrder();
        }
        return t;
    }

    /** Returns true if this is empty
     */
    @Override
    public boolean isEmpty() {
        if(front==back)
            return true;
        else
            return false;
    }

    /** Returns the capacity of the queue
     */
    @Override
    public int length() {
        return queue.length;
    }

    /** Returns the number of objects in the queue
     */
    @Override
    public int size() {
        return back;
    }

    /** Empty the queue
     */
    @Override
    public void empty() {
        back=0;
    }

    private void reOrder(){
        for(int i =0; i<queue.length; i++)
            if(queue[i]!=null)
                addClone(queue[i]);
        queue=clone;
        clone= (T[]) new Object[queue.length];
    }

    private void addClone(T t){
        for(int i =0; i<clone.length;i++)
            if(clone[i]==null) {
                clone[i] = t;
                break;
            }
    }
}
