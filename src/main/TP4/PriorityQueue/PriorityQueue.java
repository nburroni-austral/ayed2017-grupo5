package main.TP4.PriorityQueue;


import main.TP4.Queue.DynamicQueue;

public class PriorityQueue {

    private DynamicQueue[] queues;
    private int priorityLevels;

    /** Creates a PriorityQueue.
     */
    public PriorityQueue(int priorityLevels){
        if (priorityLevels <= 0){
            throw new NotAValidPriorityLevelException();
        }
        queues = new DynamicQueue[priorityLevels];
        this.priorityLevels = priorityLevels;
        for (int i = 0; i < queues.length; i++){
            queues[i] = new DynamicQueue<Object>();
        }
    }

    /** Returns the number of priority levels.
     */
    public int getNumberOfPriorityLevels() {
        return priorityLevels;
    }

    /** Adds an object into a level of priority.
     */
    public void enqueue(Object objectToEnqueue, int objectPriorityLevel){
        queues[objectPriorityLevel - 1].enqueue(objectToEnqueue);
    }

    /** Returns the first object of the highest priority level and it takes it away from the queue.
     */
    public Object dequeue(){
        if (isEmpty()){
            System.out.println("La cola de prioridades está vacía");
            return null;
        }
        int i = 0;
        while (isPriorityLevelEmpty(i + 1)){
            i++;
        }
        return queues[i].dequeue();
    }

    /** Returns whether the PriorityQueue is empty or not.
     */
    public boolean isEmpty(){
        for (int i = 1; i <= queues.length; i++){
            if (!isPriorityLevelEmpty(i)){
                return false;
            }
        }
        return true;
    }

    /** Returns whether the priority level queue is empty or not.
     */
    public boolean isPriorityLevelEmpty(int priorityLevel){
        return queues[priorityLevel - 1].isEmpty();
    }

    public void addPriorityLevel(int levelToAdd){
        if (levelToAdd <= 0 || levelToAdd > priorityLevels + 1){
            System.out.println("No se puede agregar ese nivel ya que no está dentro del rango");
        } else {
            priorityLevels++;
            DynamicQueue[] aux = new DynamicQueue[priorityLevels];
            for (int i = 0; i < levelToAdd - 1; i++){
                aux[i] = queues[i];
            }
            aux[levelToAdd - 1] = new DynamicQueue();
            for (int i = levelToAdd; i < aux.length; i++){
                aux[i] = queues[i - 1];
            }
            queues = aux;
        }
    }

    /** Empties the PriorityQueue.
     */
    public void empty(){
        for (int i = 1; i <= queues.length; i++){
            emptyPriorityLevel(i);
        }
    }

    /** Empties a certain priority level queue.
     */
    public void emptyPriorityLevel(int levelToEmpty){
        if (levelToEmpty <= 0 || levelToEmpty > priorityLevels){
            System.out.println("No se puede vaciar ese nivel ya que no está dentro del rango");
        } else {
            queues[levelToEmpty - 1].empty();
        }
    }

}
