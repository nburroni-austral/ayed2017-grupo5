package main.TP4.PriorityQueue;


public class PriorityQueueTester {

    public static void main(String[] args) {
        PriorityQueue pQueue1 = new PriorityQueue(4);
        System.out.println(pQueue1.isEmpty());
        System.out.println(pQueue1.getNumberOfPriorityLevels());
        pQueue1.enqueue(3, 1);
        pQueue1.enqueue(2, 3);
        pQueue1.enqueue(5,1);
        pQueue1.enqueue(4, 2);
        System.out.println(pQueue1.isEmpty());
        System.out.println(pQueue1.isPriorityLevelEmpty(4));
        System.out.println(pQueue1.isPriorityLevelEmpty(2));
        pQueue1.emptyPriorityLevel(2);
        System.out.println(pQueue1.isPriorityLevelEmpty(2));
        System.out.println(pQueue1.dequeue());
        pQueue1.addPriorityLevel(1);
        System.out.println(pQueue1.getNumberOfPriorityLevels());
        System.out.println(pQueue1.isPriorityLevelEmpty(1));
        pQueue1.empty();
        System.out.println(pQueue1.isEmpty());
    }



}
