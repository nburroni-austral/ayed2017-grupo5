package main.TP4.PriorityQueue;


public class NotAValidPriorityLevelException extends RuntimeException{

    public NotAValidPriorityLevelException(){
        super("La cantidad de niveles de prioridad debe ser un número entero mayor a 0");
    }
}
